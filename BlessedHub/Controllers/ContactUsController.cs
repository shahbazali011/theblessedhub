﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Core.RepositoriesInterfaces;
using BlessedHub.Models;

namespace BlessedHub.Controllers
{
    public class ContactUsController : Controller
    {
        //private TheBlessedHubEntities db = new TheBlessedHubEntities();
        private readonly IUnitOfWork _unitOfWork;

        public ContactUsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: ContactUs
        public ActionResult Index()
        {
            return View(_unitOfWork.contactUsRepository.GetContactUs());
        }

        // GET: ContactUs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContactU contactU = _unitOfWork.contactUsRepository.GetContactU(id);
            if (contactU == null)
            {
                return HttpNotFound();
            }
            return View(contactU);
        }

        // GET: ContactUs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ContactUs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Email,Phone,Message")] ContactU contactU)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.contactUsRepository.Add(contactU);
                _unitOfWork.Complete();
                return RedirectToAction("Index","Home");
            }

            return View(contactU);
        }

        // GET: ContactUs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContactU contactU = _unitOfWork.contactUsRepository.GetContactU(id);
            if (contactU == null)
            {
                return HttpNotFound();
            }
            return View(contactU);
        }

        // POST: ContactUs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Email,Phone,Message")] ContactU contactU)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.DbStateModified(contactU);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }
            return View(contactU);
        }

        // GET: ContactUs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContactU contactU = _unitOfWork.contactUsRepository.GetContactU(id);
            if (contactU == null)
            {
                return HttpNotFound();
            }
            return View(contactU);
        }

        // POST: ContactUs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ContactU contactU = _unitOfWork.contactUsRepository.GetContactU(id);
           _unitOfWork.contactUsRepository.Remove(contactU);
            _unitOfWork.Complete();
            return RedirectToAction("Index");
        }

      
    }
}
