﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Models;
using BlessedHub.ViewModels;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using BlessedHub.Core.RepositoriesInterfaces;

namespace BlessedHub.Controllers
{
    public class ServicesTypesController : Controller
    {
        //private TheBlessedHubEntities db = new TheBlessedHubEntities();
        private readonly IUnitOfWork _unitOfWork;

        public ServicesTypesController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        // GET: ServicesTypes
        public ActionResult Index()
        {
            var servicesTypes = _unitOfWork.ServiceTypes.GetServicesTypes();
            var vm = servicesTypes.Select(Mapper.Map<ServicesType, ServicesTypeVM>);
            return View(vm);
        }

        // GET: ServicesTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var servicesType = _unitOfWork.ServiceTypes.GetServicesType(id);
            if (servicesType == null)
            {
                return HttpNotFound();
            }
            var servicesTypeVM = Mapper.Map<ServicesTypeVM>(servicesType);
            return View(servicesTypeVM);
        }

        // GET: ServicesTypes/Create
        public ActionResult Create()
        {
            ViewBag.ServiceId = new SelectList(_unitOfWork.Services.GetServices(), "ServiceId", "ServiceName");
            return View();
        }

        // POST: ServicesTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ServiceTypeId,ServiceType,ServiceId,Description,Active")] ServicesType servicesType)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.ServiceTypes.Add(servicesType);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }

            ViewBag.ServiceId = new SelectList(_unitOfWork.Services.GetServices(), "ServiceId", "ServiceName", servicesType.ServiceId);
            return View(servicesType);
        }

        // GET: ServicesTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var servicesType = _unitOfWork.ServiceTypes.GetServicesType(id);
            if (servicesType == null)
            {
                return HttpNotFound();
            }
            ViewBag.ServiceId = new SelectList(_unitOfWork.Services.GetServices(), "ServiceId", "ServiceName", servicesType.ServiceId);
            var servicesTypeVM = Mapper.Map<ServicesTypeVM>(servicesType);
            return View(servicesTypeVM);
        }

        // POST: ServicesTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ServiceTypeId,ServiceType,ServiceId,Description,Active")] ServicesType servicesType)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.DbStateModified(servicesType);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }
            ViewBag.ServiceId = new SelectList(_unitOfWork.Services.GetServices(), "ServiceId", "ServiceName", servicesType.ServiceId);
            return View(servicesType);
        }

        // GET: ServicesTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var servicesType = _unitOfWork.ServiceTypes.GetServicesType(id);
            if (servicesType == null)
            {
                return HttpNotFound();
            }

            var servicesTypeVM = Mapper.Map<ServicesTypeVM>(servicesType);

            return View(servicesTypeVM);
        }

        // POST: ServicesTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var servicesType = _unitOfWork.ServiceTypes.GetServicesType(id);
            try {
                _unitOfWork.ServiceTypes.Remove(servicesType);
                _unitOfWork.Complete();
            }
            catch { }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
