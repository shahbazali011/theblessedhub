﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Core.RepositoriesInterfaces;
using BlessedHub.Models;
using BlessedHub.ViewModels;
using AutoMapper;
using AutoMapper.QueryableExtensions;

namespace BlessedHub.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        public HomeController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;

        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult PartialDaysServicesDetails(int? id)
        {
            var workingdays = _unitOfWork.ServiceTypeWorkingDaysData.GetServiceTypeWorkingDays();
            return PartialView("_PartialDaysServicesDetailsView", workingdays.Where(d=>d.DayId==id).ToList());
        }
        public ActionResult EventsLive()
        {
            var events = _unitOfWork.eventsRepository.GetEvents();
            return PartialView("_PartialEventsLive", events);
        }
        public ActionResult PartialDaysServices(int? serviceid)
        {
            var days = _unitOfWork.weekDayRepository.GetWeekDays();
            return PartialView("_PartialDaysServices",days);
        }
        public ActionResult OrganisationAll()
        {
            var organisations = _unitOfWork.OrganisationServices.GetOrganisationServices();
        
            return View("OrganisationAll", organisations);
        }
        public ActionResult ServicesAll()
        {
            var services = _unitOfWork.OrganisationServices.GetOrganisationServices();

            return View("ServicesAll", services);
        }
        public ActionResult Search(OrganisationService os)
        {
            if (os == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            string postcode = os.Organisation.PostCode;
            int serviceid = os.ServiceId;
                       
            return RedirectToAction("DetailsCitySearch", "Home", new { id = serviceid, postcode });
        }
        public ActionResult MasjidTimeTable(int? masjidid)
        {
            var praytime = _unitOfWork.prayerTimingRepository.GetPrayerTiming(masjidid);
            return View("MasjidPrayerTimeTable", praytime);
        }
        public ActionResult MasjidList()
        {
            var masjidlist = _unitOfWork.prayerTimingRepository.GetPrayerTimings().ToList();
            return View("MasjidsList", masjidlist);
        }
        public ActionResult DetailsCitySearch(int? id, string postcode)
        {
           
            Service service = _unitOfWork.Services.GetService(id);

            if (service == null)
            {
                return View("Index");
            }

            var ViewModel = new ViewModels.PostCodeServiceSearch
            {
                ServiceId = service.ServiceId,
                ServiceName = service.ServiceName,
                Description = service.Description,
                PostCode = postcode
            };

            if (service == null)
            {
                return HttpNotFound();
            }
            return View(ViewModel);
        }
        public ActionResult PartialMapOrganisationPostCode(int? id, string postcode)
        {

            var Organisation = _unitOfWork.OrganisationServices.GetOrganisationServicesMapPostCode(id,postcode);
            string markers = "[";
            foreach (var sdr in Organisation)
            {
                markers += "{";
                markers += string.Format("'title': '{0}',", sdr.Organisation.Name);
                markers += string.Format("'city': '{0}',", sdr.Organisation.City);
                markers += string.Format("'lat': '{0}',", sdr.Organisation.Latitude);
                markers += string.Format("'lng': '{0}'", sdr.Organisation.Longitude);
                markers += "},";
            }
            markers += "];";


            ViewBag.Markers = markers;

            return PartialView("_PartialMapOrganisationPostCode");
        }
        public ActionResult PartialCitySearch(int? id)
        {
            ////int DDLValue = Convert.ToInt32(Request.Form["CityId"]);

            if (id == null)
            {
                 id = Convert.ToInt32(Request.Form["CityId"]);
            }
            City city = _unitOfWork.Cities.GetCity(id);
            if (city == null)
            {
                return HttpNotFound();
            }
            return PartialView("_PartialCitySearch", city);
        }
        public ActionResult PartialMapOrganisationCity(int? id)
        {

            var Organisation = _unitOfWork.Organisations.PartialMapOrganisationCity(id);
            string markers = "[";
            foreach (var sdr in Organisation)
            {
                markers += "{";
                markers += string.Format("'title': '{0}',", sdr.Name);
                markers += string.Format("'city': '{0}',", sdr.City);
                markers += string.Format("'lat': '{0}',", sdr.Latitude);
                markers += string.Format("'lng': '{0}'", sdr.Longitude);
                markers += "},";
            }
            markers += "];";


            ViewBag.Markers = markers;

            return PartialView("_PartialMapOrganisationCity");
        }
        public ActionResult Help()
        {
           
            var giveHelpToOrganisaitons = _unitOfWork.giveHelpToOrganisaitonRepository.GetGiveHelpToOrganisaitons();
            return View(giveHelpToOrganisaitons);
        }
        public ActionResult PartialHelpItems()
        {
         
            var giveHelpToOrganisaitons = _unitOfWork.giveHelpToOrganisaitonRepository.GetGiveHelpToOrganisaitons(4);
            return PartialView("_PartialHelpItems", giveHelpToOrganisaitons.ToList());
        }
        public ActionResult PartialHelpTime()
        {
          
            var giveHelpToOrganisaitons = _unitOfWork.giveHelpToOrganisaitonRepository.GetGiveHelpToOrganisaitons(2);
            return PartialView("_PartialHelpTime", giveHelpToOrganisaitons.ToList());

        }
        public ActionResult PartialHelpMoney()
        {
           
            var giveHelpToOrganisaitons = _unitOfWork.giveHelpToOrganisaitonRepository.GetGiveHelpToOrganisaitons(1);
            return PartialView("_PartialHelpMoney", giveHelpToOrganisaitons.ToList());
        }
        public ActionResult PartialCityListSearch()
        {
            ViewBag.CityId = new SelectList(_unitOfWork.Cities.GetCities(), "CityId", "CityName");

            return PartialView("_PartialCityListSearch");
        }
        public ActionResult PartialServicesListSearch()
        {
            ViewBag.ServiceId = new SelectList(_unitOfWork.Services.GetServices(), "ServiceId", "ServiceName");

            return PartialView("_PartialServicesListSearch");
        }
        public ActionResult NewsStories()
        {
            var news = _unitOfWork.newsRepository.GetNewss();
            return View("NewsStories", news.ToList());
        }
        public ActionResult OrganisaitonServicesTypesAll(int? OrganisationId, int? ServiceId)
        {

            return PartialView("_PartialOrganisationServicesTypes", _unitOfWork.OrganisaitonServicesTypes.OrganisaitonServicesTypesAll(OrganisationId, ServiceId));
        }
        public ActionResult PartialServicesTypesCard(int? OrganisationId, int? OrganisationServicesId)
        {

            return PartialView("_PartialServicesTypesCard", _unitOfWork.OrganisaitonServicesTypes.PartialServicesTypesCard(OrganisationId, OrganisationServicesId));
        }
        public ActionResult OrganisationServicesAll(int? id)
        {

            var orginsationservices = _unitOfWork.OrganisationServices.OrganisationServicesAll(id);

            return PartialView("_PartialOrganisationServicecs", orginsationservices.Where(a=>a.Organisation.Active==true));
        }
        public ActionResult OranisationTimeTable(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var organisationWorkingDays = _unitOfWork.OrganisationWorkingDays.OranisationTimeTable(id);

            if (organisationWorkingDays == null)
            {
                return HttpNotFound();
            }
            return PartialView("_PartialOrganisaitonTimeTable", organisationWorkingDays);

        }

        //public ActionResult EventTimeTable(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    var eventsTimeTables = db.EventsTimeTables.Include(e => e.EventType).Include(e => e.WeekDay);

        //    if (eventsTimeTables == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return PartialView("_PartialEventsTimeTable", eventsTimeTables);

        //}
        public ActionResult PartialAllServicesTypesDetails(int? id)
        {
            //var test = _unitOfWork.ServiceTypeWorkingDaysData.PartialAllServicesTypesDetails(id).ToList();
            return PartialView("_PartialAllServicesTypesDetails", _unitOfWork.ServiceTypeWorkingDaysData.PartialAllServicesTypesDetails(id).ToList());

        }
        public ActionResult PartialMapOrganisation(int? id)
        {
           
            var Organisation = _unitOfWork.Organisations.PartialMapOrganisation();
            string markers = "[";
            foreach (var sdr in Organisation)
            {
                markers += "{";
                markers += string.Format("'title': '{0}',", sdr.OrganisationId);
                markers += string.Format("'city': '{0}',", sdr.City);
                markers += string.Format("'lat': '{0}',", sdr.Latitude);
                markers += string.Format("'lng': '{0}'", sdr.Longitude);
                markers += "},";
            }
            markers += "];";


            ViewBag.Markers = markers;

            return PartialView("_PartialMapOrganisation");
        }
        public ActionResult PartialMapOrganisationServices(int? id)
        {
                        var Organisation = _unitOfWork.OrganisationServices.GetOrganisationServicesMap(id);
            string markers = "[";
            foreach (var sdr in Organisation)
            {
                markers += "{";
                markers += string.Format("'title': '{0}',", sdr.Organisation.Name);
                markers += string.Format("'city': '{0}',", sdr.Organisation.City);
                markers += string.Format("'lat': '{0}',", sdr.Organisation.Latitude);
                markers += string.Format("'lng': '{0}'", sdr.Organisation.Longitude);
                markers += "},";
            }
            markers += "];";


            ViewBag.Markers = markers;

            return PartialView("_PartialMapOrganisationServices");
        }                     
        public ActionResult PartialServiceTypeTimeTable(int? ServiceTypeId, int? ServiceId, int? OrganisationId)
        {
            if (ServiceTypeId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var serviceTypeWorkingDays = _unitOfWork.ServiceTypeWorkingDaysData.PartialServiceTypeTimeTable(ServiceTypeId, ServiceId, OrganisationId);
            if (serviceTypeWorkingDays == null)
            {
                return HttpNotFound();
            }
            return PartialView("_PartialServiceTypeTimeTable", serviceTypeWorkingDays.ToList());
        }
        public ActionResult FindHelpMenue()
        {
            return PartialView("_PartialFindHelp", _unitOfWork.Services.GetServices());

        }
        public ActionResult PartialSearchService(string search)
        {

            var services = _unitOfWork.ServiceTypes.GetServicesTypeSearch(search);


            return View("_PartialSearchItemsList", services);

        }
        public ActionResult FindLocationsMenu()
        {
            return PartialView("_PartialFindLocation", _unitOfWork.Cities.GetCities());
        }
        public ActionResult PartialWeekDasys(int? id)
        {
            return PartialView("_PartialWeekDays", _unitOfWork.weekDayRepository.GetWeekDays());
        }
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Service service = _unitOfWork.Services.GetService(id);
            if (service == null)
            {
                return HttpNotFound();
            }
            return View(service);
        }
        public ActionResult OrganisationDetails(int? organisationId,int? serviceid )
        {
            if (organisationId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Organisation organisation = _unitOfWork.Organisations.GetOrganisation(organisationId);
            if (organisation == null)
            {
                return HttpNotFound();
            }
            return View(organisation);
        }
        public ActionResult OrganisationCharityDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GiveHelpToOrganisaiton giveHelpToOrganisaiton = _unitOfWork.giveHelpToOrganisaitonRepository.GiveHelpToOrganisaiton(id);
            if (giveHelpToOrganisaiton == null)
            {
                return HttpNotFound();
            }
            return View("OrganisationCharityDetail", giveHelpToOrganisaiton);
        }
        public ActionResult PartialiCanHelp(int? OrganisationId)
        {
            ICanHelp canHelp = _unitOfWork.iCanHelpRepository.GetICanHelp(OrganisationId);
            //ViewBag.GiveHelpToOrganisaitonId = new SelectList(_unitOfWork.giveHelpToOrganisaitonRepository.GetGiveHelpToOrganisaitons(), "Id", "Description");
            ViewBag.GiveHelpToOrganisaitonId = OrganisationId;
            return PartialView("_PartialiCanHelp", canHelp);
        }
        public ActionResult CountAll()
        {
            var vm = new CountAllVM
            {
                CountServices = _unitOfWork.countAll.CountServices(),
                CountOrganisations = _unitOfWork.countAll.CountOrganisations(),
                CountNeeds=_unitOfWork.countAll.CountNeeds()
            };
            return PartialView("_PartialCountall",vm);
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}