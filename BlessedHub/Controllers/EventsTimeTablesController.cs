﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Models;

namespace BlessedHub.Controllers
{
    public class EventsTimeTablesController : Controller
    {
        private TheBlessedHubEntities db = new TheBlessedHubEntities();

        // GET: EventsTimeTables
        public ActionResult Index()
        {
            var eventsTimeTables = db.EventsTimeTables.Include(e => e.EventType).Include(e => e.WeekDay);
            return View(eventsTimeTables.ToList());
        }

        // GET: EventsTimeTables/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EventsTimeTable eventsTimeTable = db.EventsTimeTables.Find(id);
            if (eventsTimeTable == null)
            {
                return HttpNotFound();
            }
            return View(eventsTimeTable);
        }

        // GET: EventsTimeTables/Create
        public ActionResult Create()
        {
            ViewBag.EventId = new SelectList(db.EventTypes, "Id", "Type");
            ViewBag.DayId = new SelectList(db.WeekDays, "Id", "Days");
            return View();
        }

        // POST: EventsTimeTables/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,EventId,DayId,StartTime,EndTime")] EventsTimeTable eventsTimeTable)
        {
            if (ModelState.IsValid)
            {
                db.EventsTimeTables.Add(eventsTimeTable);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.EventId = new SelectList(db.EventTypes, "Id", "Type", eventsTimeTable.EventId);
            ViewBag.DayId = new SelectList(db.WeekDays, "Id", "Days", eventsTimeTable.DayId);
            return View(eventsTimeTable);
        }

        // GET: EventsTimeTables/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EventsTimeTable eventsTimeTable = db.EventsTimeTables.Find(id);
            if (eventsTimeTable == null)
            {
                return HttpNotFound();
            }
            ViewBag.EventId = new SelectList(db.EventTypes, "Id", "Type", eventsTimeTable.EventId);
            ViewBag.DayId = new SelectList(db.WeekDays, "Id", "Days", eventsTimeTable.DayId);
            return View(eventsTimeTable);
        }

        // POST: EventsTimeTables/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,EventId,DayId,StartTime,EndTime")] EventsTimeTable eventsTimeTable)
        {
            if (ModelState.IsValid)
            {
                db.Entry(eventsTimeTable).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EventId = new SelectList(db.EventTypes, "Id", "Type", eventsTimeTable.EventId);
            ViewBag.DayId = new SelectList(db.WeekDays, "Id", "Days", eventsTimeTable.DayId);
            return View(eventsTimeTable);
        }

        // GET: EventsTimeTables/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EventsTimeTable eventsTimeTable = db.EventsTimeTables.Find(id);
            if (eventsTimeTable == null)
            {
                return HttpNotFound();
            }
            return View(eventsTimeTable);
        }

        // POST: EventsTimeTables/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EventsTimeTable eventsTimeTable = db.EventsTimeTables.Find(id);
            db.EventsTimeTables.Remove(eventsTimeTable);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
