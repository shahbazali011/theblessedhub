﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Models;
using BlessedHub.ViewModels;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using BlessedHub.Core.RepositoriesInterfaces;

namespace BlessedHub.Controllers
{
    public class ICanHelpsController : Controller
    {
        //private TheBlessedHubEntities db = new TheBlessedHubEntities();
        private readonly IUnitOfWork _unitOfWork;

        public ICanHelpsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: ICanHelps
        public ActionResult Index()
        {
            var iCanHelps = _unitOfWork.iCanHelpRepository.GetICanHelps();

            var vm = iCanHelps.Select(Mapper.Map<ICanHelp, ICanHelpVM>);
            return View(vm);
        }

        // GET: ICanHelps/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iCanHelp = _unitOfWork.iCanHelpRepository.GetICanHelp(id);
            if (iCanHelp == null)
            {
                return HttpNotFound();
            }

            var canHelpVM = Mapper.Map<ICanHelpVM>(iCanHelp);

            return View(canHelpVM);
        }

        // GET: ICanHelps/Create
        public ActionResult Create()
        {
            ViewBag.GiveHelpToOrganisaitonId = new SelectList(_unitOfWork.giveHelpToOrganisaitonRepository.GetGiveHelpToOrganisaitons(), "Id", "Description");
            return View();
        }

        // POST: ICanHelps/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,GiveHelpToOrganisaitonId,Email,Message,Descrption")] ICanHelp iCanHelp)
        {
            if (ModelState.IsValid)
            {
                //db.ICanHelps.Add(iCanHelp);
                //db.SaveChanges();
                _unitOfWork.iCanHelpRepository.Add(iCanHelp);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }

            ViewBag.GiveHelpToOrganisaitonId = new SelectList(_unitOfWork.giveHelpToOrganisaitonRepository.GetGiveHelpToOrganisaitons(), "Id", "Description", iCanHelp.GiveHelpToOrganisaitonId);
            return View(iCanHelp);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreatePartial([Bind(Include = "Id,GiveHelpToOrganisaitonId,Email,Message,Descrption")] ICanHelp iCanHelp)
        {
            if (ModelState.IsValid)
            {
                //db.ICanHelps.Add(iCanHelp);
                //db.SaveChanges();
                _unitOfWork.iCanHelpRepository.Add(iCanHelp);
                _unitOfWork.Complete();
                return RedirectToAction("Help","Home");
            }

            ViewBag.GiveHelpToOrganisaitonId = new SelectList(_unitOfWork.giveHelpToOrganisaitonRepository.GetGiveHelpToOrganisaitons(), "Id", "Description", iCanHelp.GiveHelpToOrganisaitonId);
            return View(iCanHelp);
        }


        // GET: ICanHelps/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iCanHelp = _unitOfWork.iCanHelpRepository.GetICanHelp(id);
            if (iCanHelp == null)
            {
                return HttpNotFound();
            }
            ViewBag.GiveHelpToOrganisaitonId = new SelectList(_unitOfWork.giveHelpToOrganisaitonRepository.GetGiveHelpToOrganisaitons(), "Id", "Description", iCanHelp.GiveHelpToOrganisaitonId);
            var canHelpVM = Mapper.Map<ICanHelpVM>(iCanHelp);

            return View(canHelpVM);
        }

        // POST: ICanHelps/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,GiveHelpToOrganisaitonId,Email,Message,Descrption")] ICanHelp iCanHelp)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(iCanHelp).State = EntityState.Modified;
                //db.SaveChanges();
                _unitOfWork.DbStateModified(iCanHelp);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }
            ViewBag.GiveHelpToOrganisaitonId = new SelectList(_unitOfWork.giveHelpToOrganisaitonRepository.GetGiveHelpToOrganisaitons(), "Id", "Description", iCanHelp.GiveHelpToOrganisaitonId);
            return View(iCanHelp);
        }

        // GET: ICanHelps/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iCanHelp = _unitOfWork.iCanHelpRepository.GetICanHelp(id);
            if (iCanHelp == null)
            {
                return HttpNotFound();
            }
            var canHelpVM = Mapper.Map<ICanHelpVM>(iCanHelp);

            return View(canHelpVM);
        }

        // POST: ICanHelps/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var iCanHelp = _unitOfWork.iCanHelpRepository.GetICanHelp(id);
            try
            {
                //db.ICanHelps.Remove(iCanHelp);
                //db.SaveChanges();
                _unitOfWork.iCanHelpRepository.Remove(iCanHelp);
            }
            catch { }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
