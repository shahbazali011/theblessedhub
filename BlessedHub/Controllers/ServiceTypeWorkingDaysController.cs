﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Models;
using BlessedHub.ViewModels;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using BlessedHub.Core.RepositoriesInterfaces;

namespace BlessedHub.Controllers
{
    public class ServiceTypeWorkingDaysController : Controller
    {
        //private TheBlessedHubEntities db = new TheBlessedHubEntities();
        private readonly IUnitOfWork _unitOfWork;

        public ServiceTypeWorkingDaysController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        // GET: ServiceTypeWorkingDays
        public ActionResult Index()
        {
            var serviceTypeWorkingDays = _unitOfWork.ServiceTypeWorkingDaysData.GetServiceTypeWorkingDays();

            var vm = serviceTypeWorkingDays.Select(Mapper.Map<ServiceTypeWorkingDay, ServiceTypeWorkingDayVM>);
            return View(vm);
        }

        // GET: ServiceTypeWorkingDays/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var serviceTypeWorkingDay = _unitOfWork.ServiceTypeWorkingDaysData.GetWorkingDay(id);
            if (serviceTypeWorkingDay == null)
            {
                return HttpNotFound();
            }
            var serviceTypeWorkingDayVM = Mapper.Map<ServiceTypeWorkingDayVM>(serviceTypeWorkingDay);
            return View(serviceTypeWorkingDayVM);
        }

        // GET: ServiceTypeWorkingDays/Create
        public ActionResult Create()
        {
            ViewBag.OrganisationId = new SelectList(_unitOfWork.Organisations.GetOrganisations(), "OrganisationId", "Name");
            ViewBag.ServiceId = new SelectList(_unitOfWork.Services.GetServices(), "ServiceId", "ServiceName");
            ViewBag.ServiceTypeId = new SelectList(_unitOfWork.ServiceTypes.GetServicesTypes(), "ServiceTypeId", "ServiceType");
            ViewBag.DayId = new SelectList(_unitOfWork.weekDayRepository.GetWeekDays(), "Id", "Days");
            return View();
        }

        // POST: ServiceTypeWorkingDays/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,DayId,ServiceId,ServiceTypeId,OrganisationId,WorkingDay,StartTime,EndTime")] ServiceTypeWorkingDay serviceTypeWorkingDay)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.ServiceTypeWorkingDaysData.Add(serviceTypeWorkingDay);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }

            ViewBag.OrganisationId = new SelectList(_unitOfWork.Organisations.GetOrganisations(), "OrganisationId", "Name", serviceTypeWorkingDay.OrganisationId);
            ViewBag.ServiceId = new SelectList(_unitOfWork.Services.GetServices(), "ServiceId", "ServiceName", serviceTypeWorkingDay.ServiceId);
            ViewBag.ServiceTypeId = new SelectList(_unitOfWork.ServiceTypes.GetServicesTypes(), "ServiceTypeId", "ServiceType", serviceTypeWorkingDay.ServiceTypeId);
            ViewBag.DayId = new SelectList(_unitOfWork.weekDayRepository.GetWeekDays(), "Id", "Days", serviceTypeWorkingDay.DayId);
            return View(serviceTypeWorkingDay);
        }

        // GET: ServiceTypeWorkingDays/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var serviceTypeWorkingDay = _unitOfWork.ServiceTypeWorkingDaysData.GetWorkingDay(id);
            if (serviceTypeWorkingDay == null)
            {
                return HttpNotFound();
            }
            ViewBag.OrganisationId = new SelectList(_unitOfWork.Organisations.GetOrganisations().Where(x => x.OrganisationId == serviceTypeWorkingDay.OrganisationId), "OrganisationId", "Name", serviceTypeWorkingDay.OrganisationId);
            ViewBag.ServiceId = new SelectList(_unitOfWork.Services.GetServices().Where(s=>s.ServiceId== serviceTypeWorkingDay.ServiceId), "ServiceId", "ServiceName", serviceTypeWorkingDay.ServiceId);
            ViewBag.ServiceTypeId = new SelectList(_unitOfWork.ServiceTypes.GetServicesTypes().Where(s=>s.ServiceTypeId== serviceTypeWorkingDay.ServiceTypeId), "ServiceTypeId", "ServiceType", serviceTypeWorkingDay.ServiceTypeId);
            ViewBag.DayId = new SelectList(_unitOfWork.weekDayRepository.GetWeekDays().Where(d=>d.Id==serviceTypeWorkingDay.DayId), "Id", "Days", serviceTypeWorkingDay.DayId);
            var serviceTypeWorkingDayVM = Mapper.Map<ServiceTypeWorkingDayVM>(serviceTypeWorkingDay);
            return View(serviceTypeWorkingDayVM);
        }

        // POST: ServiceTypeWorkingDays/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,DayId,ServiceId,ServiceTypeId,OrganisationId,WorkingDay,StartTime,EndTime")] ServiceTypeWorkingDay serviceTypeWorkingDay)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.DbStateModified(serviceTypeWorkingDay);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }
            ViewBag.OrganisationId = new SelectList(_unitOfWork.Organisations.GetOrganisations(), "OrganisationId", "Name", serviceTypeWorkingDay.OrganisationId);
            ViewBag.ServiceId = new SelectList(_unitOfWork.Services.GetServices(), "ServiceId", "ServiceName", serviceTypeWorkingDay.ServiceId);
            ViewBag.ServiceTypeId = new SelectList(_unitOfWork.ServiceTypes.GetServicesTypes(), "ServiceTypeId", "ServiceType", serviceTypeWorkingDay.ServiceTypeId);
            ViewBag.DayId = new SelectList(_unitOfWork.weekDayRepository.GetWeekDays(), "Id", "Days", serviceTypeWorkingDay.DayId);
            return View(serviceTypeWorkingDay);
        }

        // GET: ServiceTypeWorkingDays/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var serviceTypeWorkingDay = _unitOfWork.ServiceTypeWorkingDaysData.GetWorkingDay(id);
            if (serviceTypeWorkingDay == null)
            {
                return HttpNotFound();
            }
            var serviceTypeWorkingDayVM = Mapper.Map<ServiceTypeWorkingDayVM>(serviceTypeWorkingDay);
            return View(serviceTypeWorkingDayVM);
        }

        // POST: ServiceTypeWorkingDays/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var serviceTypeWorkingDay = _unitOfWork.ServiceTypeWorkingDaysData.GetWorkingDay(id);
            try {
                _unitOfWork.ServiceTypeWorkingDaysData.Remove(serviceTypeWorkingDay);
                _unitOfWork.Complete();
            }
            catch { }
            return RedirectToAction("Index");
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        //db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
