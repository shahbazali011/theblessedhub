﻿using BlessedHub.Core.RepositoriesInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BlessedHub.Controllers
{
    public class AboutController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public AboutController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Organisation()
        {
            return View();
        }

        public ActionResult OurTeam()
        {
            return View(_unitOfWork.blessedHubMembers.GetBlessedHubMembers());
        }
        public ActionResult OurTrustees()
        {
            return View(_unitOfWork.blessedHubMembers.GetBlessedHubMembers());
        }
        public ActionResult Jobs()
        {
            return View();
        }
        public ActionResult ContactUs()
        {
            return View();
        }
        public ActionResult FAQs()
        {
            return View();
        }
    }
}