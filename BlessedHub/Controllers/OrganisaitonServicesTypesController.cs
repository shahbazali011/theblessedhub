﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Models;
using BlessedHub.ViewModels;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using BlessedHub.Core.RepositoriesInterfaces;

namespace BlessedHub.Controllers
{
    public class OrganisaitonServicesTypesController : Controller
    {
        //private TheBlessedHubEntities db = new TheBlessedHubEntities();
        private readonly IUnitOfWork _unitOfWork;

        public OrganisaitonServicesTypesController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        // GET: OrganisaitonServicesTypes
        public ActionResult Index()
        {
            var organisaitonServicesTypes = _unitOfWork.OrganisaitonServicesTypes.GetOrganisaitonServicesTypes();
            var vm = organisaitonServicesTypes.Select(Mapper.Map<OrganisaitonServicesType, OrganisaitonServicesTypeVM>);
            return View(vm);
        }

        // GET: OrganisaitonServicesTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var organisaitonServicesType = _unitOfWork.OrganisaitonServicesTypes.GetOrganisaitonServicesType(id);
            if (organisaitonServicesType == null)
            {
                return HttpNotFound();
            }
            var organisaitonServicesTypeVM = Mapper.Map<OrganisaitonServicesTypeVM>(organisaitonServicesType);
            return View(organisaitonServicesTypeVM);
        }

    


        // GET: OrganisaitonServicesTypes/Create
        public ActionResult Create()
        {
            ViewBag.OrganisationId = new SelectList(_unitOfWork.Organisations.GetOrganisations(), "OrganisationId", "Name");
            ViewBag.ServiceId = new SelectList(_unitOfWork.Services.GetServices(), "ServiceId", "ServiceName");
            ViewBag.ServiceTypeId = new SelectList(_unitOfWork.ServiceTypes.GetServicesTypes(), "ServiceTypeId", "ServiceType");
            return View();
        }

        // POST: OrganisaitonServicesTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "OrganisaitonServicesTypesId,ServiceTypeId,OrganisationId,SuitableFor,Description,ServiceId,DateAdded,Active,DateUpdated")] OrganisaitonServicesType organisaitonServicesType)
        {
            if (ModelState.IsValid)
            {
                organisaitonServicesType.DateAdded = DateTime.Now;
                _unitOfWork.OrganisaitonServicesTypes.Add(organisaitonServicesType);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }

            ViewBag.OrganisationId = new SelectList(_unitOfWork.Organisations.GetOrganisations(), "OrganisationId", "Name", organisaitonServicesType.OrganisationId);
            ViewBag.ServiceId = new SelectList(_unitOfWork.Services.GetServices(), "ServiceId", "ServiceName", organisaitonServicesType.ServiceId);
  
            ViewBag.ServiceTypeId = new SelectList(_unitOfWork.ServiceTypes.GetServicesTypes(), "ServiceTypeId", "ServiceType", organisaitonServicesType.ServiceTypeId);
            return View(organisaitonServicesType);
        }

        // GET: OrganisaitonServicesTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var organisaitonServicesType = _unitOfWork.OrganisaitonServicesTypes.GetOrganisaitonServicesType(id);
            if (organisaitonServicesType == null)
            {
                return HttpNotFound();
            }
            ViewBag.OrganisationId = new SelectList(_unitOfWork.Organisations.GetOrganisations().Where(x=>x.OrganisationId==id), "OrganisationId", "Name", organisaitonServicesType.OrganisationId);
            ViewBag.ServiceId = new SelectList(_unitOfWork.OrganisationServices.GetOrganisationServices().Where(y => y.OrganisationId == id).Select(s=>s.Service), "ServiceId", "ServiceName", organisaitonServicesType.ServiceId);
            ViewBag.ServiceTypeId = new SelectList(_unitOfWork.OrganisaitonServicesTypes.GetOrganisaitonServicesTypes().Where(x=>x.OrganisationId==id).Select(s=>s.ServicesType), "ServiceTypeId", "ServiceType", organisaitonServicesType.ServiceTypeId);
            var organisaitonServicesTypeVM = Mapper.Map<OrganisaitonServicesTypeVM>(organisaitonServicesType);
            return View(organisaitonServicesTypeVM);
        }

        // POST: OrganisaitonServicesTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OrganisaitonServicesTypesId,ServiceTypeId,OrganisationId,SuitableFor,Description,ServiceId,DateAdded,Active,DateUpdated")] OrganisaitonServicesType organisaitonServicesType)
        {
            if (ModelState.IsValid)
            {
                organisaitonServicesType.DateUpdated = DateTime.Now;
                _unitOfWork.DbStateModified(organisaitonServicesType);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }
            ViewBag.OrganisationId = new SelectList(_unitOfWork.Organisations.GetOrganisations(), "OrganisationId", "Name", organisaitonServicesType.OrganisationId);
            ViewBag.ServiceId = new SelectList(_unitOfWork.Services.GetServices(), "ServiceId", "ServiceName", organisaitonServicesType.ServiceId);
            ViewBag.ServiceTypeId = new SelectList(_unitOfWork.ServiceTypes.GetServicesTypes(), "ServiceTypeId", "ServiceType", organisaitonServicesType.ServiceTypeId);
            return View(organisaitonServicesType);
        }

        // GET: OrganisaitonServicesTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var organisaitonServicesType = _unitOfWork.OrganisaitonServicesTypes.GetOrganisaitonServicesType(id);
            if (organisaitonServicesType == null)
            {
                return HttpNotFound();
            }
            var organisaitonServicesTypeVM = Mapper.Map<OrganisaitonServicesTypeVM>(organisaitonServicesType);
            return View(organisaitonServicesTypeVM);
        }

        // POST: OrganisaitonServicesTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var organisaitonServicesType = _unitOfWork.OrganisaitonServicesTypes.GetOrganisaitonServicesType(id);
            try { 
            _unitOfWork.OrganisaitonServicesTypes.Remove(organisaitonServicesType);
                _unitOfWork.Complete();
            }
            catch { }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
