﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Models;
using BlessedHub.ViewModels;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using BlessedHub.Core.RepositoriesInterfaces;

namespace BlessedHub.Controllers
{
    public class NewsTypesController : Controller
    {
        //private TheBlessedHubEntities db = new TheBlessedHubEntities();
        private readonly IUnitOfWork _unitOfWork;

        public NewsTypesController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        //
        // GET: NewsTypes
        public ActionResult Index()
        {
            var vm = _unitOfWork.newsTypeRepository.GetNewsTypes().Select(Mapper.Map<NewsType, NewsTypeVM>);
            return View(vm);
        }

        // GET: NewsTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var newsType = _unitOfWork.newsTypeRepository.GetNewsType(id);
            if (newsType == null)
            {
                return HttpNotFound();
            }
            var newsTypeVM = Mapper.Map<NewsTypeVM>(newsType);
            return View(newsTypeVM);
        }

        // GET: NewsTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: NewsTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,NewsType1,Description")] NewsType newsType)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.newsTypeRepository.Add(newsType);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }

            return View(newsType);
        }

        // GET: NewsTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var newsType = _unitOfWork.newsTypeRepository.GetNewsType(id);
            if (newsType == null)
            {
                return HttpNotFound();
            }
            var newsTypeVM = Mapper.Map<NewsTypeVM>(newsType);
            return View(newsTypeVM);
        }

        // POST: NewsTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,NewsType1,Description")] NewsType newsType)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.DbStateModified(newsType);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }
            return View(newsType);
        }

        // GET: NewsTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var newsType = _unitOfWork.newsTypeRepository.GetNewsType(id);
            if (newsType == null)
            {
                return HttpNotFound();
            }
            var newsTypeVM = Mapper.Map<NewsTypeVM>(newsType);
            return View(newsTypeVM);
        }

        // POST: NewsTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var newsType = _unitOfWork.newsTypeRepository.GetNewsType(id);
            try {
                _unitOfWork.newsTypeRepository.Remove(newsType);
                _unitOfWork.Complete();
            }
            catch { }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
