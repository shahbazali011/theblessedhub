﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Models;

namespace BlessedHub.Controllers
{
    public class MasjidDetailsController : Controller
    {
        private TheBlessedHubEntities db = new TheBlessedHubEntities();

        // GET: MasjidDetails
        public ActionResult Index()
        {
            return View(db.MasjidDetails.ToList());
        }

        // GET: MasjidDetails/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MasjidDetail masjidDetail = db.MasjidDetails.Find(id);
            if (masjidDetail == null)
            {
                return HttpNotFound();
            }
            return View(masjidDetail);
        }

        // GET: MasjidDetails/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MasjidDetails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,MasjidName,Address,PostCode,Phone,ImamName,ImamMobile")] MasjidDetail masjidDetail)
        {
            if (ModelState.IsValid)
            {
                db.MasjidDetails.Add(masjidDetail);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(masjidDetail);
        }

        // GET: MasjidDetails/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MasjidDetail masjidDetail = db.MasjidDetails.Find(id);
            if (masjidDetail == null)
            {
                return HttpNotFound();
            }
            return View(masjidDetail);
        }

        // POST: MasjidDetails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,MasjidName,Address,PostCode,Phone,ImamName,ImamMobile")] MasjidDetail masjidDetail)
        {
            if (ModelState.IsValid)
            {
                db.Entry(masjidDetail).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(masjidDetail);
        }

        // GET: MasjidDetails/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MasjidDetail masjidDetail = db.MasjidDetails.Find(id);
            if (masjidDetail == null)
            {
                return HttpNotFound();
            }
            return View(masjidDetail);
        }

        // POST: MasjidDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MasjidDetail masjidDetail = db.MasjidDetails.Find(id);
            db.MasjidDetails.Remove(masjidDetail);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
