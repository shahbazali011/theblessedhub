﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BlessedHub.Controllers
{
    public class FooterController : Controller
    {
        // GET: Footer
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult PrivacyPolicy()
        {
            return View();
        }

        public ActionResult Legacy()
        {
            return View();
        }

        public ActionResult Lessons()
        {
            return View();
        }

        public ActionResult Journey()
        {
            return View();
        }

        public ActionResult PursuitOfHappiness()
        {
            return View();
        }
    }
}