﻿using BlessedHub.Core.RepositoriesInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BlessedHub.Controllers
{
    public class JsonController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public JsonController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        // GET: Json
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetServicesTypesForServiceId(string id)
        {
            var serviceid = Convert.ToInt32(id);

            return Json(_unitOfWork.ServiceTypes.GetServicesTypes().Where(y => y.ServiceId == serviceid).Select(x => new
            { servicetypeid = x.ServiceTypeId, servicetype = x.ServiceType }).ToList());

        }
        public JsonResult GetServicesForOrgId(string id)
        {
            if (id == "")
            {
                return Json(_unitOfWork.OrganisationServices.GetOrganisationServices().Where(y => y.OrganisationId == 0).Select(x => new
                { serviceid = x.Service.ServiceId, service = x.Service.ServiceName }).ToList());
            }
            var orgid = Convert.ToInt32(id);
            return Json(_unitOfWork.OrganisationServices.GetOrganisationServices().Where(y => y.OrganisationId == orgid).Select(x => new
            { serviceid = x.Service.ServiceId, service = x.Service.ServiceName }).ToList());

        }
        public JsonResult GetHelpSubTypesForId(string id)
        {
            if (id == "")
            {
                return Json(_unitOfWork.giveHelpToOrganisaitonRepository.GetGiveHelpToOrganisaitons().Where(y => y.OrganisationId == 0).Select(x => new
                { id = x.GiveHelpSubType.Id, type = x.GiveHelpSubType.HelpSubType }).ToList());
            }
            var orgid = Convert.ToInt32(id);

            //var test =_unitOfWork.giveHelpToOrganisaitonRepository.GetGiveHelpToOrganisaitons().Where(y => y.OrganisationId == orgid);

            //return Json(_unitOfWork.giveHelpToOrganisaitonRepository.GetGiveHelpToOrganisaitons().Where(y => y.OrganisationId == orgid).Select(x => new
            //{ id = x.GiveHelpSubType.Id, type = x.GiveHelpSubType.HelpSubType }).ToList());
            return Json(_unitOfWork.givehelpSubTypeRepository.GetGivehelpSubTypes().Where(y => y.GiveHelpTypesId == orgid).Select(x => new
            { id = x.Id, type = x.HelpSubType }).ToList());
        }
        public JsonResult GetNewsSubTypes(string id)
        {
            var newsid = Convert.ToInt32(id);

            return Json(_unitOfWork.newsSubTypeRepository.GetNewsSubTypes().Where(y => y.NewsTypeId == newsid).Select(x => new
            { id = x.Id, subtype = x.NewsSubType1 }).ToList());

        }
    }
    }
