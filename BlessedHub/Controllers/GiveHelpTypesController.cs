﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Models;
using BlessedHub.ViewModels;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using BlessedHub.Core.RepositoriesInterfaces;

namespace BlessedHub.Controllers
{
    public class GiveHelpTypesController : Controller
    {
        //private TheBlessedHubEntities db = new TheBlessedHubEntities();
        private readonly IUnitOfWork _unitOfWork;

        public GiveHelpTypesController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: GiveHelpTypes
        public ActionResult Index()
        {
            //var giveHelpTypes = db.GiveHelpTypes.ProjectTo<GiveHelpTypeVM>().ToList();
            var giveHelpTypes = _unitOfWork.giveHelpTypeRepository.GetGiveHelpTypes();
            var vm = giveHelpTypes.Select(Mapper.Map<GiveHelpType, GiveHelpTypeVM>);

            return View(vm);
        }

        // GET: GiveHelpTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //GiveHelpType giveHelpType = db.GiveHelpTypes.Find(id);
            var giveHelpType = _unitOfWork.giveHelpTypeRepository.GetGiveHelpType(id);
            if (giveHelpType == null)
            {
                return HttpNotFound();
            }
            var giveHelpTypeVM = Mapper.Map<GiveHelpTypeVM>(giveHelpType);
            return View(giveHelpTypeVM);
        }

        // GET: GiveHelpTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: GiveHelpTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,HelpType,Description")] GiveHelpType giveHelpType)
        {
            if (ModelState.IsValid)
            {
                //db.GiveHelpTypes.Add(giveHelpType);
                //db.SaveChanges();
                _unitOfWork.giveHelpTypeRepository.Add(giveHelpType);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }

            return View(giveHelpType);
        }

        // GET: GiveHelpTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var giveHelpType = _unitOfWork.giveHelpTypeRepository.GetGiveHelpType(id);
            if (giveHelpType == null)
            {
                return HttpNotFound();
            }
            var giveHelpTypeVM = Mapper.Map<GiveHelpTypeVM>(giveHelpType);
            return View(giveHelpTypeVM);
        }

        // POST: GiveHelpTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,HelpType,Description")] GiveHelpType giveHelpType)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(giveHelpType).State = EntityState.Modified;
                //db.SaveChanges();
                _unitOfWork.DbStateModified(giveHelpType);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }
            return View(giveHelpType);
        }

        // GET: GiveHelpTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var giveHelpType = _unitOfWork.giveHelpTypeRepository.GetGiveHelpType(id);

            if (giveHelpType == null)
            {
                return HttpNotFound();
            }
            var giveHelpTypeVM = Mapper.Map<GiveHelpTypeVM>(giveHelpType);
            return View(giveHelpTypeVM);
        }

        // POST: GiveHelpTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var giveHelpType = _unitOfWork.giveHelpTypeRepository.GetGiveHelpType(id);

            try
            {
                //db.GiveHelpTypes.Remove(giveHelpType);
                //db.SaveChanges();
                _unitOfWork.giveHelpTypeRepository.Remove(giveHelpType);
                _unitOfWork.Complete();
            }
            catch { }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
