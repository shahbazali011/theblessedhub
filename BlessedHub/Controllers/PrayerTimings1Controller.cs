﻿  using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using BlessedHub.Core.RepositoriesInterfaces;
using BlessedHub.Models;
using BlessedHub.ViewModels;

namespace BlessedHub.Controllers
{
    
    public class PrayerTimings1Controller : Controller
    {
        private TheBlessedHubEntities db = new TheBlessedHubEntities();
        private readonly IUnitOfWork _unitOfWork;
        public PrayerTimings1Controller(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        // GET: PrayerTimings1
        public ActionResult Index()
        {
            return View(_unitOfWork.prayerTimingRepository.GetPrayerTimings().ToList());

        }
        // GET: PrayerTimings1/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var prayerTiming = _unitOfWork.prayerTimingRepository.GetPrayerTiming(id);
            if (prayerTiming == null)
            {
                return HttpNotFound();
            }
            var ptVM = Mapper.Map<PrayerTimingVM>(prayerTiming);
            return View(ptVM);
        }
        // GET: PrayerTimings1/Create
        public ActionResult Create()
        {
            ViewBag.MasjidId = new SelectList(db.MasjidDetails, "Id", "MasjidName");
            return View();
        }
        // POST: PrayerTimings1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,MasjidId,CalendarPicURL,DateAdded,DateUpdated")] PrayerTiming prayerTiming)
        {
            if (ModelState.IsValid)
            {
                db.PrayerTimings.Add(prayerTiming);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.MasjidId = new SelectList(db.MasjidDetails, "Id", "MasjidName", prayerTiming.MasjidId);
            return View(prayerTiming);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreatePrayerTimings(PrayerTimingVM vmModel)
        {
            string filePath = string.Empty;
            string fileContentType = string.Empty;
            string filename = string.Empty;

            try
            {
                // Verification
                if (ModelState.IsValid)
                {
                    // Converting to bytes.
                    byte[] uploadedFile = new byte[vmModel.FileAttach.InputStream.Length];
                    vmModel.FileAttach.InputStream.Read(uploadedFile, 0, uploadedFile.Length);

                    // Initialization.
                    fileContentType = vmModel.FileAttach.ContentType;

                    filename = vmModel.MasjidId + ".png";

                    //string folderPath = "~/Images/PrayerTimings/";
                    //this.WriteBytesToFile(this.Server.MapPath(folderPath), uploadedFile, filename);

                    //https://member3-3.smarterasp.net/cp/filemanager?d=h:/root/home/shahbazali-001/www/theblessedhub/images/prayertimings/
                    string folderPath = "/Images/PrayerTimings/";
                    //filePath = Path.Combine(Server.MapPath(folderPath), filename);
                    this.WriteBytesToFile(this.Server.MapPath(folderPath), uploadedFile, filename);
                    filePath = folderPath + filename;                                     
                }
                //Mapper.Map(vmModel,test);

                var prayerTiming = new PrayerTiming
                {
                    MasjidId = vmModel.MasjidId,
                    DateAdded = vmModel.DateAdded,
                    DateUpdated = vmModel.DateUpdated,
                    CalendarPicURL = filePath



                };

                if (ModelState.IsValid)
                {

                    var prytime = _unitOfWork.prayerTimingRepository.GetMasjidByName(prayerTiming.CalendarPicURL);

                    if(prytime ==null)
                    {
                        _unitOfWork.prayerTimingRepository.Add(prayerTiming);
                        _unitOfWork.Complete();

                    }

                   
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                // Info
                Console.Write(ex);
            }
            return View("Index");
        }
        private void WriteBytesToFile(string rootFolderPath, byte[] fileBytes, string filename)
        {
            try
            {
                // Verification.
                if (!Directory.Exists(rootFolderPath))
                {
                    // Initialization.
                    string fullFolderPath = rootFolderPath;

                    // Settings.
                    string folderPath = new Uri(fullFolderPath).LocalPath;

                    // Create.
                    Directory.CreateDirectory(folderPath);
                }

                // Initialization.                
                string fullFilePath = rootFolderPath + filename;

                // Create.
                FileStream fs = System.IO.File.Create(fullFilePath);

                // Close.
                fs.Flush();
                fs.Dispose();
                fs.Close();

                // Write Stream.
                BinaryWriter sw = new BinaryWriter(new FileStream(fullFilePath, FileMode.Create, FileAccess.Write));

                // Write to file.
                sw.Write(fileBytes);

                // Closing.
                sw.Flush();
                sw.Dispose();
                sw.Close();
            }
            catch (Exception ex)
            {
                // Info.
                throw ex;
            }
        }
        // GET: PrayerTimings1/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PrayerTiming prayerTiming = _unitOfWork.prayerTimingRepository.GetPrayerTiming(id);
            if (prayerTiming == null)
            {
                return HttpNotFound();
            }
            var ptVM = Mapper.Map<PrayerTimingVM>(prayerTiming);
            ViewBag.MasjidId = new SelectList(db.MasjidDetails, "Id", "MasjidName", prayerTiming.MasjidId);
            return View(ptVM);
        }
        // POST: PrayerTimings1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,MasjidId,CalendarPicURL,DateAdded,DateUpdated")] PrayerTiming prayerTiming)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.DbStateModified(prayerTiming);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }
            ViewBag.MasjidId = new SelectList(db.MasjidDetails, "Id", "MasjidName", prayerTiming.MasjidId);
            return View(prayerTiming);
        }
        // GET: PrayerTimings1/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var prayerTiming = _unitOfWork.prayerTimingRepository.GetPrayerTiming(id);
            if (prayerTiming == null)
            {
                return HttpNotFound();
            }
            var ptVM = Mapper.Map<PrayerTimingVM>(prayerTiming);
            return View(ptVM);
        }
        // POST: PrayerTimings1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PrayerTiming prayerTiming = _unitOfWork.prayerTimingRepository.GetPrayerTiming(id);
            _unitOfWork.prayerTimingRepository.Remove(prayerTiming);
            _unitOfWork.Complete();
            return RedirectToAction("Index");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
