﻿//////using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Models;
using BlessedHub.ViewModels;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using BlessedHub.Core.RepositoriesInterfaces;

namespace BlessedHub.Controllers
{
    public class CitiesController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public CitiesController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        // GET: Cities
        public ActionResult Index()
        {
            var cities = _unitOfWork.Cities.GetCities();
            var cityVM = cities.Select(Mapper.Map<City, CityVM>);
            

            return View(cityVM);
        }

        // GET: Cities/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var city = _unitOfWork.Cities.GetCity(id);
            if (city == null)
            {
                return HttpNotFound();
            }
            var cityVM = Mapper.Map<CityVM>(city);

            return View(cityVM);
        }

        // GET: Cities/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Cities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CityId,CityName,DateAdded,Active,DateUpdated")] City city)
        {
            if (ModelState.IsValid)
            {
                //city.DateAdded = DateTime.Now;

                _unitOfWork.Cities.Add(city);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }

            return View(city);
        }

        // GET: Cities/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var city = _unitOfWork.Cities.GetCity(id);
                                 
            if (city == null)
            {
                return HttpNotFound();
            }
            var cityVM = Mapper.Map<CityVM>(city);

            return View(cityVM);
        }

        // POST: Cities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CityId,CityName,DateAdded,Active,DateUpdated")] City city)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(city).State = EntityState.Modified;
                _unitOfWork.DbStateModified(city);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }
            return View(city);
        }

        // GET: Cities/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //City city = db.Cities.Find(id);
            var city = _unitOfWork.Cities.GetCity(id);
            if (city == null)
            {
                return HttpNotFound();
            }
            var cityVM = Mapper.Map<CityVM>(city);
            return View(cityVM);
        }

        // POST: Cities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var city = _unitOfWork.Cities.GetCity(id);
            try
            {
            
                _unitOfWork.Cities.Remove(city);
                _unitOfWork.Complete();
            }
            catch
            {

            }
            
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
