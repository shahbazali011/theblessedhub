﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using BlessedHub.Core.RepositoriesInterfaces;
using BlessedHub.Models;
using BlessedHub.ViewModels;

namespace BlessedHub.Controllers
{
    public class BlessedHubMembersController : Controller
    {

        private readonly IUnitOfWork _unitOfWork;

        public BlessedHubMembersController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: BlessedHubMembers
        public ActionResult Index()
        {
            var blessedHubMembers = _unitOfWork.blessedHubMembers.GetBlessedHubMembers();
            return View(blessedHubMembers.ToList());
        }

        // GET: BlessedHubMembers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BlessedHubMember blessedHubMember = _unitOfWork.blessedHubMembers.GetBlessedHubMember(id);
            if (blessedHubMember == null)
            {
                return HttpNotFound();
            }
            return View(blessedHubMember);
        }

        private void WriteBytesToFile(string rootFolderPath, byte[] fileBytes, string filename)
        {
            try
            {
                // Verification.
                if (!Directory.Exists(rootFolderPath))
                {
                    // Initialization.
                    string fullFolderPath = rootFolderPath;

                    // Settings.
                    string folderPath = new Uri(fullFolderPath).LocalPath;

                    // Create.
                    Directory.CreateDirectory(folderPath);
                }

                // Initialization.                
                string fullFilePath = rootFolderPath + filename;

                // Create.
                FileStream fs = System.IO.File.Create(fullFilePath);

                // Close.
                fs.Flush();
                fs.Dispose();
                fs.Close();

                // Write Stream.
                BinaryWriter sw = new BinaryWriter(new FileStream(fullFilePath, FileMode.Create, FileAccess.Write));

                // Write to file.
                sw.Write(fileBytes);

                // Closing.
                sw.Flush();
                sw.Dispose();
                sw.Close();
            }
            catch (Exception ex)
            {
                // Info.
                throw ex;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateMember(BlessedHubMemberVM vmModel)
        {
            string filePath = string.Empty;
            string fileContentType = string.Empty;
            
            try
            {
                // Verification
                if (ModelState.IsValid)
                {
                    // Converting to bytes.
                    byte[] uploadedFile = new byte[vmModel.FileAttach.InputStream.Length];
                    vmModel.FileAttach.InputStream.Read(uploadedFile, 0, uploadedFile.Length);

                    // Initialization.
                    fileContentType = vmModel.FileAttach.ContentType;
                    string folderPath = "/Images/Members/";
                    this.WriteBytesToFile(this.Server.MapPath(folderPath), uploadedFile, vmModel.FileAttach.FileName);
                    filePath = folderPath + vmModel.FileAttach.FileName;
                    // Saving info.
                }

                //Mapper.Map(vmModel,test);
                
                var blessedHubMember = new BlessedHubMember
                {
                    FirstName = vmModel.FirstName,
                    LastName = vmModel.LastName,
                    JobTitle = vmModel.JobTitle,
                    JobDescription = vmModel.JobDescription,
                    TeamId = vmModel.TeamId,
                    Mobiel = vmModel.Mobiel,
                    Phone = vmModel.Phone,
                    Role = vmModel.Role,
                    Acievement = vmModel.Acievement,
                    Address = vmModel.Address,
                    Postcode = vmModel.Postcode,
                    City = vmModel.City,
                    RoleId = vmModel.RoleId,
                    Active = vmModel.Active,
                    DateAdded = vmModel.DateAdded,
                    DateUpdated = vmModel.DateUpdated,
                    PictureUrl = filePath
                    
                   
                };

                if (ModelState.IsValid)
                {
                    _unitOfWork.blessedHubMembers.Add(blessedHubMember);
                    _unitOfWork.Complete();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                // Info
                Console.Write(ex);
            }

            return View();
        }
        // GET: BlessedHubMembers/Create
        public ActionResult Create()
        {
            ViewBag.TeamId = new SelectList(_unitOfWork.blessedHubTeam.GetBleassedHubTeams(), "Id", "TeamTitle");
            return View();
        }

        // POST: BlessedHubMembers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,FirstName,LastName,JobTitle,JobDescription,TeamId,Mobiel,Phone,Role,Acievement,Address,Postcode,City,RoleId,Active,DateAdded,DateUpdated")] BlessedHubMember blessedHubMember)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.blessedHubMembers.Add(blessedHubMember);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }

            ViewBag.TeamId = new SelectList(_unitOfWork.blessedHubTeam.GetBleassedHubTeams(), "Id", "TeamTitle", blessedHubMember.TeamId);
            return View(blessedHubMember);
        }

        // GET: BlessedHubMembers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BlessedHubMember blessedHubMember = _unitOfWork.blessedHubMembers.GetBlessedHubMember(id);
            if (blessedHubMember == null)
            {
                return HttpNotFound();
            }
            ViewBag.TeamId = new SelectList(_unitOfWork.blessedHubTeam.GetBleassedHubTeams(), "Id", "TeamTitle", blessedHubMember.TeamId);
            var blessedHubMemberVM = new BlessedHubMemberVM
            {
                Id=blessedHubMember.Id,
                FirstName = blessedHubMember.FirstName,
                LastName = blessedHubMember.LastName,
                JobTitle = blessedHubMember.JobTitle,
                JobDescription = blessedHubMember.JobDescription,
                TeamId = blessedHubMember.TeamId,
                Mobiel = blessedHubMember.Mobiel,
                Phone = blessedHubMember.Phone,
                Role = blessedHubMember.Role,
                Acievement = blessedHubMember.Acievement,
                Address = blessedHubMember.Address,
                Postcode = blessedHubMember.Postcode,
                City = blessedHubMember.City,
                RoleId = blessedHubMember.RoleId,
                Active = blessedHubMember.Active,
                DateAdded = blessedHubMember.DateAdded,
                DateUpdated = blessedHubMember.DateUpdated,
                PictureUrl = blessedHubMember.PictureUrl


            };
            return View(blessedHubMemberVM);
        }

        // POST: BlessedHubMembers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(BlessedHubMemberVM vmModel)
        {
            string filePath = string.Empty;
            string fileContentType = string.Empty;

            try
            {
                // Verification
                if (ModelState.IsValid)
                {
                    // Converting to bytes.
                    byte[] uploadedFile = new byte[vmModel.FileAttach.InputStream.Length];
                    vmModel.FileAttach.InputStream.Read(uploadedFile, 0, uploadedFile.Length);

                    // Initialization.
                    fileContentType = vmModel.FileAttach.ContentType;
                    string folderPath = "/Images/Members/";
                    this.WriteBytesToFile(this.Server.MapPath(folderPath), uploadedFile, vmModel.FileAttach.FileName);
                    filePath = folderPath + vmModel.FileAttach.FileName;

                    // Saving info.
                }

                //Mapper.Map(vmModel,test);

                var blessedHubMember = new BlessedHubMember
                {
                    Id=vmModel.Id,
                    FirstName = vmModel.FirstName,
                    LastName = vmModel.LastName,
                    JobTitle = vmModel.JobTitle,
                    JobDescription = vmModel.JobDescription,
                    TeamId = vmModel.TeamId,
                    Mobiel = vmModel.Mobiel,
                    Phone = vmModel.Phone,
                    Role = vmModel.Role,
                    Acievement = vmModel.Acievement,
                    Address = vmModel.Address,
                    Postcode = vmModel.Postcode,
                    City = vmModel.City,
                    RoleId = vmModel.RoleId,
                    Active = vmModel.Active,
                    DateAdded = vmModel.DateAdded,
                    DateUpdated = vmModel.DateUpdated,
                    PictureUrl = filePath


                };

                if (ModelState.IsValid)
                {
                    _unitOfWork.DbStateModified(blessedHubMember);
                    _unitOfWork.Complete();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                // Info
                Console.Write(ex);
            }
            ViewBag.TeamId = new SelectList(_unitOfWork.blessedHubTeam.GetBleassedHubTeams(), "Id", "TeamTitle", vmModel.TeamId);

            return View();
        }

        // GET: BlessedHubMembers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BlessedHubMember blessedHubMember = _unitOfWork.blessedHubMembers.GetBlessedHubMember(id);
            if (blessedHubMember == null)
            {
                return HttpNotFound();
            }
            return View(blessedHubMember);
        }

        // POST: BlessedHubMembers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BlessedHubMember blessedHubMember = _unitOfWork.blessedHubMembers.GetBlessedHubMember(id);
            _unitOfWork.blessedHubMembers.Remove(blessedHubMember);
            _unitOfWork.Complete();
            return RedirectToAction("Index");
        }

    }
}
