﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Models;
using BlessedHub.ViewModels;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using BlessedHub.Core.RepositoriesInterfaces;

namespace BlessedHub.Controllers
{
    public class WeekDaysController : Controller
    {
        //private TheBlessedHubEntities db = new TheBlessedHubEntities();
        private readonly IUnitOfWork _unitOfWork;
        public WeekDaysController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        // GET: WeekDays
        public ActionResult Index()
        {
            return View(_unitOfWork.weekDayRepository.GetWeekDays());
        }

        // GET: WeekDays/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WeekDay weekDay = _unitOfWork.weekDayRepository.GetWeekDay(id);
            if (weekDay == null)
            {
                return HttpNotFound();
            }
            return View(weekDay);
        }

        // GET: WeekDays/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: WeekDays/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Days")] WeekDay weekDay)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.weekDayRepository.Add(weekDay);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }

            return View(weekDay);
        }

        // GET: WeekDays/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WeekDay weekDay = _unitOfWork.weekDayRepository.GetWeekDay(id);
            if (weekDay == null)
            {
                return HttpNotFound();
            }
            return View(weekDay);
        }

        // POST: WeekDays/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Days")] WeekDay weekDay)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.DbStateModified(weekDay);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }
            return View(weekDay);
        }

        // GET: WeekDays/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WeekDay weekDay = _unitOfWork.weekDayRepository.GetWeekDay(id);
           
            return View(weekDay);
        }

        // POST: WeekDays/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            WeekDay weekDay = _unitOfWork.weekDayRepository.GetWeekDay(id);

            try { 
          _unitOfWork.weekDayRepository.Remove(weekDay);
                _unitOfWork.Complete();
        }
            catch { }
            return RedirectToAction("Index");
        }

      
    }
}
