﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Models;
using BlessedHub.ViewModels;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using BlessedHub.Core.RepositoriesInterfaces;

namespace BlessedHub.Controllers
{
    public class OrganisationWorkingDaysController : Controller
    {
        //private TheBlessedHubEntities db = new TheBlessedHubEntities();
        private readonly IUnitOfWork _unitOfWork;

        public OrganisationWorkingDaysController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        // GET: OrganisationWorkingDays
        public ActionResult Index()
        {
            var organisationWorkingDays = _unitOfWork.OrganisationWorkingDays.GetOrganisationWorkingDays();
            var vm = organisationWorkingDays.Select(Mapper.Map<OrganisationWorkingDay, OrganisationWorkingDayVM>);
            return View(vm);
        }

        // GET: OrganisationWorkingDays/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var organisationWorkingDay = _unitOfWork.OrganisationWorkingDays.GetWorkingDay(id);
            if (organisationWorkingDay == null)
            {
                return HttpNotFound();
            }
            var organisationWorkingDayVM = Mapper.Map<OrganisationWorkingDayVM>(organisationWorkingDay);
            return View(organisationWorkingDayVM);
        }

        // GET: OrganisationWorkingDays/Create
        public ActionResult Create()
        {
            ViewBag.OrganisationId = new SelectList(_unitOfWork.Organisations.GetOrganisations(), "OrganisationId", "Name");
            ViewBag.DayId = new SelectList(_unitOfWork.weekDayRepository.GetWeekDays(), "Id", "Days");
            return View();
        }

        // POST: OrganisationWorkingDays/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,DayId,OrganisationId,WorkingDay,StartTime,EndTime")] OrganisationWorkingDay organisationWorkingDay)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.OrganisationWorkingDays.Add(organisationWorkingDay);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }

            ViewBag.OrganisationId = new SelectList(_unitOfWork.Organisations.GetOrganisations(), "OrganisationId", "Name", organisationWorkingDay.OrganisationId);
            ViewBag.DayId = new SelectList(_unitOfWork.weekDayRepository.GetWeekDays(), "Id", "Days", organisationWorkingDay.DayId);
            return View(organisationWorkingDay);
        }

        // GET: OrganisationWorkingDays/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var organisationWorkingDay = _unitOfWork.OrganisationWorkingDays.GetWorkingDay(id);
            if (organisationWorkingDay == null)
            {
                return HttpNotFound();
            }
            ViewBag.OrganisationId = new SelectList(_unitOfWork.Organisations.GetOrganisations().Where(x=>x.OrganisationId== organisationWorkingDay.OrganisationId), "OrganisationId", "Name", organisationWorkingDay.OrganisationId);
            ViewBag.DayId = new SelectList(_unitOfWork.weekDayRepository.GetWeekDays().Where(x=>x.Id==organisationWorkingDay.DayId), "Id", "Days", organisationWorkingDay.DayId);
            var organisationWorkingDayVM = Mapper.Map<OrganisationWorkingDayVM>(organisationWorkingDay);
            return View(organisationWorkingDayVM);
        }

        // POST: OrganisationWorkingDays/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,DayId,OrganisationId,WorkingDay,StartTime,EndTime")] OrganisationWorkingDay organisationWorkingDay)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.DbStateModified(organisationWorkingDay);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }
            ViewBag.OrganisationId = new SelectList(_unitOfWork.Organisations.GetOrganisations(), "OrganisationId", "Name", organisationWorkingDay.OrganisationId);
            ViewBag.DayId = new SelectList(_unitOfWork.weekDayRepository.GetWeekDays(), "Id", "Days", organisationWorkingDay.DayId);
            return View(organisationWorkingDay);
        }

        // GET: OrganisationWorkingDays/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var organisationWorkingDay = _unitOfWork.OrganisationWorkingDays.GetWorkingDay(id);
            if (organisationWorkingDay == null)
            {
                return HttpNotFound();
            }
            var organisationWorkingDayVM = Mapper.Map<OrganisationWorkingDayVM>(organisationWorkingDay);
            return View(organisationWorkingDayVM);
        }

        // POST: OrganisationWorkingDays/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var organisationWorkingDay = _unitOfWork.OrganisationWorkingDays.GetWorkingDay(id);
            try {
                _unitOfWork.OrganisationWorkingDays.Remove(organisationWorkingDay);
                _unitOfWork.Complete();
            }
            catch { }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
