﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Models;
using BlessedHub.ViewModels;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using BlessedHub.Core.RepositoriesInterfaces;
using System.IO;


namespace BlessedHub.Controllers
{
    public class NewsController : Controller
    {
        //private TheBlessedHubEntities db = new TheBlessedHubEntities();
        private readonly IUnitOfWork _unitOfWork;

        public NewsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: News
        public ActionResult Index()
        {
            var news = _unitOfWork.newsRepository.GetNewss();
            var vm = news.Select(Mapper.Map<News, NewsVM>);
            return View(vm);
        }
        public ActionResult NewsStories()
        {
            //var news = db.News.Include(n => n.NewsSubType).Include(n => n.NewsType);
            var news = _unitOfWork.newsRepository.GetNewsType(1);
            return View("NewsStories", news.ToList());
        }
        public ActionResult NewsArticles()
        {
            //var news = db.News.Include(n => n.NewsSubType).Include(n => n.NewsType);
            var news = _unitOfWork.newsRepository.GetNewsType(2);
            return View("NewsArticles", news.ToList());
        }
        public ActionResult NewsLatest()
        {
            //var news = db.News.Include(n => n.NewsSubType).Include(n => n.NewsType);
            var news = _unitOfWork.newsRepository.GetNewsType(3);
            return View("NewsLatest", news.ToList());
        }
        // GET: News/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var news = _unitOfWork.newsRepository.GetNews(id);
            if (news == null)
            {
                return HttpNotFound();
            }

            var newsVM = Mapper.Map<NewsVM>(news);
            return View(newsVM);
        }

        private void WriteBytesToFile(string rootFolderPath, byte[] fileBytes, string filename)
        {
            try
            {
                // Verification.
                if (!Directory.Exists(rootFolderPath))
                {
                    // Initialization.
                    string fullFolderPath = rootFolderPath;

                    // Settings.
                    string folderPath = new Uri(fullFolderPath).LocalPath;

                    // Create.
                    Directory.CreateDirectory(folderPath);
                }

                // Initialization.                
                string fullFilePath = rootFolderPath + filename;

                // Create.
                FileStream fs = System.IO.File.Create(fullFilePath);

                // Close.
                fs.Flush();
                fs.Dispose();
                fs.Close();

                // Write Stream.
                BinaryWriter sw = new BinaryWriter(new FileStream(fullFilePath, FileMode.Create, FileAccess.Write));

                // Write to file.
                sw.Write(fileBytes);

                // Closing.
                sw.Flush();
                sw.Dispose();
                sw.Close();
            }
            catch (Exception ex)
            {
                // Info.
                throw ex;
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateNews(NewsVM vmModel)
        {

            string filePath = string.Empty;
            string fileContentType = string.Empty;


            try
            {
                // Verification
                if (ModelState.IsValid)
                {
                    // Converting to bytes.
                    byte[] uploadedFile = new byte[vmModel.FileAttach.InputStream.Length];
                    vmModel.FileAttach.InputStream.Read(uploadedFile, 0, uploadedFile.Length);

                    // Initialization.
                    fileContentType = vmModel.FileAttach.ContentType;
                    string folderPath = "/Images/News/";
                    this.WriteBytesToFile(this.Server.MapPath(folderPath), uploadedFile, vmModel.FileAttach.FileName);
                    filePath = folderPath + vmModel.FileAttach.FileName;

                    // Saving info.
                }

          
                //Mapper.Map(vmModel,test);


                var news = new News
                {
                    NewsTitle = vmModel.NewsTitle,
                    NewsDetail = vmModel.NewsDetail,
                    MoreDetails = vmModel.MoreDetails,
                    PostedDate = vmModel.PostedDate,
                    NewsTypeId = vmModel.NewsTypeId,
                    NewsSubTypeId = vmModel.NewsSubTypeId,
                    FacebookLink = vmModel.FacebookLink,
                    TwitterLink = vmModel.TwitterLink,
                    Tagged = vmModel.Tagged,
                    PictureURL = filePath,
                    Active=vmModel.Active,
                    DateAdded=vmModel.DateAdded,
                    DateUpdated=vmModel.DateUpdated
                };

                if (ModelState.IsValid)
                {
                    _unitOfWork.newsRepository.Add(news);
                    _unitOfWork.Complete();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                // Info
                Console.Write(ex);
            }

            return View();
        }

        // GET: News/Create
        public ActionResult Create()
        {
            ViewBag.NewsSubTypeId = new SelectList(_unitOfWork.newsSubTypeRepository.GetNewsSubTypes(), "Id", "NewsSubType1");
            ViewBag.NewsTypeId = new SelectList(_unitOfWork.newsTypeRepository.GetNewsTypes(), "Id", "NewsType1");
            return View();
        }

        // POST: News/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,NewsTitle,NewsDetail,MoreDetails,PostedDate,PostedBy,NewsTypeId,NewsSubTypeId,FacebookLink,TwitterLink,Tagged,PictureURL,Active,DateAdded,DateUpdated")] News news)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.newsRepository.Add(news);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }

            ViewBag.NewsSubTypeId = new SelectList(_unitOfWork.newsSubTypeRepository.GetNewsSubTypes(), "Id", "NewsSubType1", news.NewsSubTypeId);
            ViewBag.NewsTypeId = new SelectList(_unitOfWork.newsTypeRepository.GetNewsTypes(), "Id", "NewsType1", news.NewsTypeId);
            return View(news);
        }

        // GET: News/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var news = _unitOfWork.newsRepository.GetNews(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            ViewBag.NewsSubTypeId = new SelectList(_unitOfWork.newsSubTypeRepository.GetNewsSubTypes(), "Id", "NewsSubType1", news.NewsSubTypeId);
            ViewBag.NewsTypeId = new SelectList(_unitOfWork.newsTypeRepository.GetNewsTypes(), "Id", "NewsType1", news.NewsTypeId);
            var newsVM = Mapper.Map<NewsVM>(news);
            return View(newsVM);
        }

        // POST: News/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(NewsVM vmModel)
        {

            string filePath = string.Empty;
            string fileContentType = string.Empty;


            try
            {
                // Verification
                if (ModelState.IsValid)
                {
                    // Converting to bytes.
                    byte[] uploadedFile = new byte[vmModel.FileAttach.InputStream.Length];
                    vmModel.FileAttach.InputStream.Read(uploadedFile, 0, uploadedFile.Length);

                    // Initialization.
                    fileContentType = vmModel.FileAttach.ContentType;
                    string folderPath = "/Images/News/";
                    this.WriteBytesToFile(this.Server.MapPath(folderPath), uploadedFile, vmModel.FileAttach.FileName);
                    filePath = folderPath + vmModel.FileAttach.FileName;

                    // Saving info.
                }              
          
               var  news = new News
                {
                   Id=vmModel.Id,
                   NewsTitle = vmModel.NewsTitle,
                    NewsDetail = vmModel.NewsDetail,
                    MoreDetails = vmModel.MoreDetails,
                    PostedDate = vmModel.PostedDate,
                    NewsTypeId = vmModel.NewsTypeId,
                    NewsSubTypeId = vmModel.NewsSubTypeId,
                    FacebookLink = vmModel.FacebookLink,
                    TwitterLink = vmModel.TwitterLink,
                    Tagged = vmModel.Tagged,
                    PictureURL = filePath,
                    Active = vmModel.Active,
                    DateAdded = vmModel.DateAdded,
                    DateUpdated = vmModel.DateUpdated
                };

                if (ModelState.IsValid)
                {
                    //_unitOfWork.newsRepository.Add(news);
                    _unitOfWork.DbStateModified(news);
                    _unitOfWork.Complete();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                // Info
                Console.Write(ex);
            }
            ViewBag.NewsSubTypeId = new SelectList(_unitOfWork.newsSubTypeRepository.GetNewsSubTypes(), "Id", "NewsSubType1", vmModel.NewsSubTypeId);
            ViewBag.NewsTypeId = new SelectList(_unitOfWork.newsTypeRepository.GetNewsTypes(), "Id", "NewsType1", vmModel.NewsTypeId);
            return View();
        }

        // GET: News/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var news = _unitOfWork.newsRepository.GetNews(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            var newsVM = Mapper.Map<NewsVM>(news);
            return View(newsVM);
        }

        // POST: News/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var news = _unitOfWork.newsRepository.GetNews(id);
            try
            {
                _unitOfWork.newsRepository.Remove(news);
                _unitOfWork.Complete();
            }
            catch { }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
