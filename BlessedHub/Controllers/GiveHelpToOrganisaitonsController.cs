﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Models;
using BlessedHub.ViewModels;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using BlessedHub.Core.RepositoriesInterfaces;

namespace BlessedHub.Controllers
{
    public class GiveHelpToOrganisaitonsController : Controller
    {
        //private TheBlessedHubEntities db = new TheBlessedHubEntities();
        private readonly IUnitOfWork _unitOfWork;

        public GiveHelpToOrganisaitonsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: GiveHelpToOrganisaitons
        public ActionResult Index()
        {
            //var giveHelpToOrganisaitons = db.GiveHelpToOrganisaitons.Include(g => g.GiveHelpSubType).Include(g => g.GiveHelpType).Include(g => g.Organisation);

            var giveHelpToOrganisaitons1 = _unitOfWork.giveHelpToOrganisaitonRepository.GetGiveHelpToOrganisaitons();
            //var vm = giveHelpToOrganisaitons.ProjectTo<GiveHelpToOrganisaitonVM>().ToList();
            var vm = giveHelpToOrganisaitons1.Select(Mapper.Map<GiveHelpToOrganisaiton, GiveHelpToOrganisaitonVM>);
            return View(vm);
        }

        // GET: GiveHelpToOrganisaitons/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //GiveHelpToOrganisaiton giveHelpToOrganisaiton = db.GiveHelpToOrganisaitons.Find(id);
            //GiveHelpToOrganisaitonVM giveHelpToOrganisaitonVM  = Mapper.Map<GiveHelpToOrganisaitonVM>(giveHelpToOrganisaiton);
            var giveHelpToOrganisaiton = _unitOfWork.giveHelpToOrganisaitonRepository.GiveHelpToOrganisaiton(id);
            if (giveHelpToOrganisaiton == null)
            {
                return HttpNotFound();
            }
            var giveHelpToOrganisaitonVM = Mapper.Map<GiveHelpToOrganisaitonVM>(giveHelpToOrganisaiton);
            return View(giveHelpToOrganisaitonVM);
        }

        // GET: GiveHelpToOrganisaitons/Create
        public ActionResult Create()
        {
            //ViewBag.GiveHelpSubTypesId = new SelectList(db.GiveHelpSubTypes, "Id", "HelpSubType");
            //ViewBag.GiveHelpTypesId = new SelectList(db.GiveHelpTypes, "Id", "HelpType");
            //ViewBag.OrganisationId = new SelectList(db.Organisations, "OrganisationId", "Name");
            ViewBag.GiveHelpSubTypesId = new SelectList(_unitOfWork.givehelpSubTypeRepository.GetGivehelpSubTypes(), "Id", "HelpSubType");
            ViewBag.GiveHelpTypesId = new SelectList(_unitOfWork.giveHelpTypeRepository.GetGiveHelpTypes(), "Id", "HelpType");
            ViewBag.OrganisationId = new SelectList(_unitOfWork.Organisations.GetOrganisations(), "OrganisationId", "Name");
            return View();
        }

        // POST: GiveHelpToOrganisaitons/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,GiveHelpSubTypesId,OrganisationId,Description,GiveHelpTypesId,DateAdded,Active,DateUpdated")] GiveHelpToOrganisaiton giveHelpToOrganisaiton)
        {
            if (ModelState.IsValid)
            {
                //db.GiveHelpToOrganisaitons.Add(giveHelpToOrganisaiton);
                //db.SaveChanges();
                _unitOfWork.giveHelpToOrganisaitonRepository.Add(giveHelpToOrganisaiton);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }

            ViewBag.GiveHelpSubTypesId = new SelectList(_unitOfWork.givehelpSubTypeRepository.GetGivehelpSubTypes(), "Id", "HelpSubType", giveHelpToOrganisaiton.GiveHelpSubTypesId);
            ViewBag.GiveHelpTypesId = new SelectList(_unitOfWork.giveHelpTypeRepository.GetGiveHelpTypes(), "Id", "HelpType", giveHelpToOrganisaiton.GiveHelpTypesId);
            ViewBag.OrganisationId = new SelectList(_unitOfWork.Organisations.GetOrganisations(), "OrganisationId", "Name", giveHelpToOrganisaiton.OrganisationId);
            return View(giveHelpToOrganisaiton);
        }

        // GET: GiveHelpToOrganisaitons/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //GiveHelpToOrganisaiton giveHelpToOrganisaiton = db.GiveHelpToOrganisaitons.Find(id);
            var giveHelpToOrganisaiton = _unitOfWork.giveHelpToOrganisaitonRepository.GiveHelpToOrganisaiton(id);

            if (giveHelpToOrganisaiton == null)
            {
                return HttpNotFound();
            }
            //GiveHelpToOrganisaitonVM giveHelpToOrganisaitonVM = Mapper.Map<GiveHelpToOrganisaitonVM>(giveHelpToOrganisaiton);

            ViewBag.GiveHelpSubTypesId = new SelectList(_unitOfWork.givehelpSubTypeRepository.GetGivehelpSubTypes().Where(s=>s.Id==giveHelpToOrganisaiton.GiveHelpSubTypesId), "Id", "HelpSubType", giveHelpToOrganisaiton.GiveHelpSubTypesId);
            ViewBag.GiveHelpTypesId = new SelectList(_unitOfWork.giveHelpTypeRepository.GetGiveHelpTypes().Where(g=>g.Id==giveHelpToOrganisaiton.GiveHelpTypesId), "Id", "HelpType", giveHelpToOrganisaiton.GiveHelpTypesId);
            ViewBag.OrganisationId = new SelectList(_unitOfWork.Organisations.GetOrganisations().Where(o=>o.OrganisationId==giveHelpToOrganisaiton.OrganisationId), "OrganisationId", "Name", giveHelpToOrganisaiton.OrganisationId);

            var giveHelpToOrganisaitonVM = Mapper.Map<GiveHelpToOrganisaitonVM>(giveHelpToOrganisaiton);
            return View(giveHelpToOrganisaitonVM);
        }

        // POST: GiveHelpToOrganisaitons/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,GiveHelpSubTypesId,OrganisationId,Description,GiveHelpTypesId,DateAdded,Active,DateUpdated")] GiveHelpToOrganisaiton giveHelpToOrganisaiton)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(giveHelpToOrganisaiton).State = EntityState.Modified;
                //db.SaveChanges();
                _unitOfWork.DbStateModified(giveHelpToOrganisaiton);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }
            ViewBag.GiveHelpSubTypesId = new SelectList(_unitOfWork.givehelpSubTypeRepository.GetGivehelpSubTypes(), "Id", "HelpSubType", giveHelpToOrganisaiton.GiveHelpSubTypesId);
            ViewBag.GiveHelpTypesId = new SelectList(_unitOfWork.giveHelpTypeRepository.GetGiveHelpTypes(), "Id", "HelpType", giveHelpToOrganisaiton.GiveHelpTypesId);
            ViewBag.OrganisationId = new SelectList(_unitOfWork.Organisations.GetOrganisations(), "OrganisationId", "Name", giveHelpToOrganisaiton.OrganisationId);
            return View(giveHelpToOrganisaiton);
        }

        // GET: GiveHelpToOrganisaitons/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //GiveHelpToOrganisaiton giveHelpToOrganisaiton = db.GiveHelpToOrganisaitons.Find(id);
            var giveHelpToOrganisaiton = _unitOfWork.giveHelpToOrganisaitonRepository.GiveHelpToOrganisaiton(id);

            if (giveHelpToOrganisaiton == null)
            {
                return HttpNotFound();
            }
            //GiveHelpToOrganisaitonVM giveHelpToOrganisaitonVM = Mapper.Map<GiveHelpToOrganisaitonVM>(giveHelpToOrganisaiton);

            var giveHelpToOrganisaitonVM = Mapper.Map<GiveHelpToOrganisaitonVM>(giveHelpToOrganisaiton);
            return View(giveHelpToOrganisaitonVM);
        }

        // POST: GiveHelpToOrganisaitons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            //GiveHelpToOrganisaiton giveHelpToOrganisaiton = db.GiveHelpToOrganisaitons.Find(id);
            var giveHelpToOrganisaiton = _unitOfWork.giveHelpToOrganisaitonRepository.GiveHelpToOrganisaiton(id);
            try
            {
                //db.GiveHelpToOrganisaitons.Remove(giveHelpToOrganisaiton);
                //db.SaveChanges();
                _unitOfWork.giveHelpToOrganisaitonRepository.Remove(giveHelpToOrganisaiton);
                _unitOfWork.Complete();
            }
            catch { }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
