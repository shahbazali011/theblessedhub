﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Core.RepositoriesInterfaces;
using BlessedHub.Models;

namespace BlessedHub.Controllers
{
    public class BleassedHubTeamsController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public BleassedHubTeamsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        // GET: BleassedHubTeams
        public ActionResult Index()
        {
            return View(_unitOfWork.blessedHubTeam.GetBleassedHubTeams());
        }

        // GET: BleassedHubTeams/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BleassedHubTeam bleassedHubTeam = _unitOfWork.blessedHubTeam.GetBleassedHubTeam(id);
            if (bleassedHubTeam == null)
            {
                return HttpNotFound();
            }
            return View(bleassedHubTeam);
        }

        // GET: BleassedHubTeams/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BleassedHubTeams/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,TeamTitle")] BleassedHubTeam bleassedHubTeam)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.blessedHubTeam.Add(bleassedHubTeam);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }

            return View(bleassedHubTeam);
        }

        // GET: BleassedHubTeams/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BleassedHubTeam bleassedHubTeam = _unitOfWork.blessedHubTeam.GetBleassedHubTeam(id);
            if (bleassedHubTeam == null)
            {
                return HttpNotFound();
            }
            return View(bleassedHubTeam);
        }

        // POST: BleassedHubTeams/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,TeamTitle")] BleassedHubTeam bleassedHubTeam)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.DbStateModified(bleassedHubTeam);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }
            return View(bleassedHubTeam);
        }

        // GET: BleassedHubTeams/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BleassedHubTeam bleassedHubTeam = _unitOfWork.blessedHubTeam.GetBleassedHubTeam(id);
            if (bleassedHubTeam == null)
            {
                return HttpNotFound();
            }
            return View(bleassedHubTeam);
        }

        // POST: BleassedHubTeams/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BleassedHubTeam bleassedHubTeam = _unitOfWork.blessedHubTeam.GetBleassedHubTeam(id);
            _unitOfWork.blessedHubTeam.Remove(bleassedHubTeam);
            _unitOfWork.Complete();
            return RedirectToAction("Index");
        }

    }
}
