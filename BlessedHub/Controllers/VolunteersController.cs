﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Models;
using BlessedHub.ViewModels;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using BlessedHub.Core.RepositoriesInterfaces;
using System.Web.Routing;

namespace BlessedHub.Controllers
{
    public class VolunteersController : Controller
    {
        //private TheBlessedHubEntities db = new TheBlessedHubEntities();
        private readonly IUnitOfWork _unitOfWork;

        //protected override void OnActionExecuting(ActionExecutingContext filterContext)
        //{
        //    base.OnActionExecuting(filterContext);
        //    var allParamsNull = filterContext.ActionParameters.All(a => a.Value == null);
        //    if (allParamsNull)
        //    {
        //        filterContext.Result = new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //}

        public VolunteersController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        // GET: Volunteers
        public ActionResult Index()
        {
            var volunteers = _unitOfWork.volunteerRepository.GetVolunteers();
            var vm = volunteers.Select(Mapper.Map<Volunteer, VolunteerVM>);
            return View(vm);
        }

        // GET: Volunteers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var volunteer = _unitOfWork.volunteerRepository.GetVolunteer(id);
            if (volunteer == null)
            {
                return HttpNotFound();
            }
            var volunteerVM = Mapper.Map<VolunteerVM>(volunteer);
            return View(volunteerVM);
        }

        // GET: Volunteers/Create
        public ActionResult Create()
        {
            ViewBag.CityId = new SelectList(_unitOfWork.Cities.GetCities(), "CityId", "CityName");
            return View();
        }

        // POST: Volunteers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,FirstName,LastName,EmailAddress,TelephoneNumber,CityId,PostCode,Skills,Availablity,Resources,DateAdded,Active,DateUpdated")] Volunteer volunteer)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.volunteerRepository.Add(volunteer);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }

            ViewBag.CityId = new SelectList(_unitOfWork.Cities.GetCities(), "CityId", "CityName", volunteer.CityId);
            return View(volunteer);
        }

        // GET: Volunteers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var volunteer = _unitOfWork.volunteerRepository.GetVolunteer(id);
            if (volunteer == null)
            {
                return HttpNotFound();
            }
            ViewBag.CityId = new SelectList(_unitOfWork.Cities.GetCities(), "CityId", "CityName", volunteer.CityId);
            var volunteerVM = Mapper.Map<VolunteerVM>(volunteer);
            return View(volunteerVM);
        }

        // POST: Volunteers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FirstName,LastName,EmailAddress,TelephoneNumber,CityId,PostCode,Skills,Availablity,Resources,DateAdded,Active,DateUpdated")] Volunteer volunteer)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.DbStateModified(volunteer);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }
            ViewBag.CityId = new SelectList(_unitOfWork.Cities.GetCities(), "CityId", "CityName", volunteer.CityId);
            return View(volunteer);
        }

        // GET: Volunteers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var volunteer = _unitOfWork.volunteerRepository.GetVolunteer(id);
            if (volunteer == null)
            {
                return HttpNotFound();
            }
            var volunteerVM = Mapper.Map<VolunteerVM>(volunteer);
            return View(volunteerVM);
        }

        // POST: Volunteers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var volunteer = _unitOfWork.volunteerRepository.GetVolunteer(id);
            try { 
            _unitOfWork.volunteerRepository.Remove(volunteer);
                _unitOfWork.Complete();
            }
            catch { }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
