﻿

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Models;
using BlessedHub.ViewModels;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using BlessedHub.Core.RepositoriesInterfaces;

namespace BlessedHub.Controllers
{
    public class ServicesController : Controller
    {
        //private TheBlessedHubEntities db = new TheBlessedHubEntities();
        private readonly IUnitOfWork _unitOfWork;

        public ServicesController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        // GET: Services
        public ActionResult Index()
        {
            var vm = _unitOfWork.Services.GetServices().Select(Mapper.Map<Service, ServiceVM>);
            return View(vm);
        }

        // GET: Services/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var service = _unitOfWork.Services.GetService(id);
            if (service == null)
            {
                return HttpNotFound();
            }
            var serviceVM = Mapper.Map<ServiceVM>(service);
            return View(serviceVM);
        }

        // GET: Services/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Services/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ServiceId,ServiceName,Description,Active")] Service service)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.Services.Add(service);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }

            return View(service);
        }

        // GET: Services/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var service = _unitOfWork.Services.GetService(id);
            if (service == null)
            {
                return HttpNotFound();
            }
            var serviceVM = Mapper.Map<ServiceVM>(service);
            return View(serviceVM);
        }

        // POST: Services/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ServiceId,ServiceName,Description,Active")] Service service)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.DbStateModified(service);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }
            return View(service);
        }

        // GET: Services/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var service = _unitOfWork.Services.GetService(id);
            if (service == null)
            {
                return HttpNotFound();
            }
            var serviceVM = Mapper.Map<ServiceVM>(service);
            return View(serviceVM);
        }

        // POST: Services/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var service = _unitOfWork.Services.GetService(id);
            try
            {
                _unitOfWork.Services.Remove(service);
                _unitOfWork.Complete();
                
            }
            catch
            {
                ModelState.AddModelError("", "This record can not be removed.");
                //return HttpNotFound();
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
