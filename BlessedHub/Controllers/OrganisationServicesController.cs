﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Models;
using BlessedHub.ViewModels;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using BlessedHub.Core.RepositoriesInterfaces;

namespace BlessedHub.Controllers
{
    public class OrganisationServicesController : Controller
    {
        //private TheBlessedHubEntities db = new TheBlessedHubEntities();
        private readonly IUnitOfWork _unitOfWork;

        public OrganisationServicesController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        // GET: OrganisationServices
        public ActionResult Index()
        {
            var organisationServices = _unitOfWork.OrganisationServices.GetOrganisationServices();
            var vm = organisationServices.Select(Mapper.Map<OrganisationService, OrganisationServiceVM>);
            return View(vm);
        }

        // GET: OrganisationServices/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var organisationService = _unitOfWork.OrganisationServices.GetOrganisationservice(id);
            if (organisationService == null)
            {
                return HttpNotFound();
            }
            var organisationServiceVM = Mapper.Map<OrganisationServiceVM>(organisationService);
            return View(organisationServiceVM);
        }

        // GET: OrganisationServices/Create
        public ActionResult Create()
        {
            ViewBag.OrganisationId = new SelectList(_unitOfWork.Organisations.GetOrganisations(), "OrganisationId", "Name");
            ViewBag.ServiceId = new SelectList(_unitOfWork.Services.GetServices(), "ServiceId", "ServiceName");
            return View();
        }

        // POST: OrganisationServices/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "OrganisationServicesId,OrganisationId,Description,ServiceId,DateAdded,Active,DateUpdated")] OrganisationService organisationService)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.OrganisationServices.Add(organisationService);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }

            ViewBag.OrganisationId = new SelectList(_unitOfWork.Organisations.GetOrganisations(), "OrganisationId", "Name", organisationService.OrganisationId);
            ViewBag.ServiceId = new SelectList(_unitOfWork.Services.GetServices(), "ServiceId", "ServiceName", organisationService.ServiceId);
            return View(organisationService);
        }

        // GET: OrganisationServices/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var organisationService = _unitOfWork.OrganisationServices.GetOrganisationservice(id);
            if (organisationService == null)
            {
                return HttpNotFound();
            }
            ViewBag.OrganisationId = new SelectList(_unitOfWork.Organisations.GetOrganisations(), "OrganisationId", "Name", organisationService.OrganisationId);
            ViewBag.ServiceId = new SelectList(_unitOfWork.Services.GetServices(), "ServiceId", "ServiceName", organisationService.ServiceId);
            var organisationServiceVM = Mapper.Map<OrganisationServiceVM>(organisationService);
            return View(organisationServiceVM);
        }

        // POST: OrganisationServices/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OrganisationServicesId,OrganisationId,Description,ServiceId,DateAdded,Active,DateUpdated")] OrganisationService organisationService)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.DbStateModified(organisationService);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }
            ViewBag.OrganisationId = new SelectList(_unitOfWork.Organisations.GetOrganisations(), "OrganisationId", "Name", organisationService.OrganisationId);
            ViewBag.ServiceId = new SelectList(_unitOfWork.Services.GetServices(), "ServiceId", "ServiceName", organisationService.ServiceId);
            return View(organisationService);
        }

        // GET: OrganisationServices/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var organisationService = _unitOfWork.OrganisationServices.GetOrganisationservice(id);
            if (organisationService == null)
            {
                return HttpNotFound();
            }
            var organisationServiceVM = Mapper.Map<OrganisationServiceVM>(organisationService);
            return View(organisationServiceVM);
        }

        // POST: OrganisationServices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var organisationService = _unitOfWork.OrganisationServices.GetOrganisationservice(id);
            try {
                _unitOfWork.OrganisationServices.Remove(organisationService);
                _unitOfWork.Complete();
            }
            catch { }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
