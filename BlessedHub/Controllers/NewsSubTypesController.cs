﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Models;
using BlessedHub.ViewModels;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using BlessedHub.Core.RepositoriesInterfaces;

namespace BlessedHub.Controllers
{
    public class NewsSubTypesController : Controller
    {
        //private TheBlessedHubEntities db = new TheBlessedHubEntities();
        private readonly IUnitOfWork _unitOfWork;

        public NewsSubTypesController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        // GET: NewsSubTypes
        public ActionResult Index()
        {
            var newsSubTypes = _unitOfWork.newsSubTypeRepository.GetNewsSubTypes();
            var vm = newsSubTypes.Select(Mapper.Map<NewsSubType, NewsSubTypeVM>);
            return View(vm);
        }

        // GET: NewsSubTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var newsSubType = _unitOfWork.newsSubTypeRepository.GetNewsSubType(id);
            if (newsSubType == null)
            {
                return HttpNotFound();
            }
            var newsSubTypeVM = Mapper.Map<NewsSubTypeVM>(newsSubType);
            return View(newsSubTypeVM);
        }

        // GET: NewsSubTypes/Create
        public ActionResult Create()
        {
            ViewBag.NewsTypeId = new SelectList(_unitOfWork.newsTypeRepository.GetNewsTypes(), "Id", "NewsType1");
            return View();
        }

        // POST: NewsSubTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,NewsTypeId,NewsSubType1,Description")] NewsSubType newsSubType)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.newsSubTypeRepository.Add(newsSubType);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }

            ViewBag.NewsTypeId = new SelectList(_unitOfWork.newsTypeRepository.GetNewsTypes(), "Id", "NewsType1", newsSubType.NewsTypeId);
            return View(newsSubType);
        }

        // GET: NewsSubTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var newsSubType = _unitOfWork.newsSubTypeRepository.GetNewsSubType(id);
            if (newsSubType == null)
            {
                return HttpNotFound();
            }
            ViewBag.NewsTypeId = new SelectList(_unitOfWork.newsTypeRepository.GetNewsTypes(), "Id", "NewsType1", newsSubType.NewsTypeId);
            var newsSubTypeVM = Mapper.Map<NewsSubTypeVM>(newsSubType);
            return View(newsSubTypeVM);
        }

        // POST: NewsSubTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,NewsTypeId,NewsSubType1,Description")] NewsSubType newsSubType)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.DbStateModified(newsSubType);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }
            ViewBag.NewsTypeId = new SelectList(_unitOfWork.newsTypeRepository.GetNewsTypes(), "Id", "NewsType1", newsSubType.NewsTypeId);
            return View(newsSubType);
        }

        // GET: NewsSubTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var newsSubType = _unitOfWork.newsSubTypeRepository.GetNewsSubType(id);
            if (newsSubType == null)
            {
                return HttpNotFound();
            }
            var newsSubTypeVM = Mapper.Map<NewsSubTypeVM>(newsSubType);
            return View(newsSubTypeVM);
        }

        // POST: NewsSubTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var newsSubType = _unitOfWork.newsSubTypeRepository.GetNewsSubType(id);
            try
            {
                _unitOfWork.newsSubTypeRepository.Remove(newsSubType);
                _unitOfWork.Complete();
            }
            catch { }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
