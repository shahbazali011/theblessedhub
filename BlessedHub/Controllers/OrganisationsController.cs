﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Models;
using BlessedHub.ViewModels;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using BlessedHub.Core.RepositoriesInterfaces;
using PagedList;

namespace BlessedHub.Controllers
{
    public class OrganisationsController : Controller
    {
        //private TheBlessedHubEntities db = new TheBlessedHubEntities();
        private readonly IUnitOfWork _unitOfWork;

        public OrganisationsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        // GET: Organisations
        public ActionResult Index(int? page)
        {
            var organisations = _unitOfWork.Organisations.GetOrganisations();
            var vm = organisations.Select(Mapper.Map<Organisation, OrganisationVM>);
            //return View(vm);

            int pageSize = 1;
            int pageNumber = (page ?? 1);
            return View(vm.ToPagedList(pageNumber, pageSize));
        
    }

        // GET: Organisations/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var organisation = _unitOfWork.Organisations.GetOrganisation(id);
          
            if (organisation == null)
            {
                return HttpNotFound();
            }
            var organisationVM = Mapper.Map<OrganisationVM>(organisation);
            return View(organisationVM);
        }

        // GET: Organisations/Create
        public ActionResult Create()
        {
            ViewBag.CityId = new SelectList(_unitOfWork.Cities.GetCities(), "CityId", "CityName");
            return View();
        }

        // POST: Organisations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "OrganisationId,CityId,Name,Type,Title,SubTitle,Description,Address1,Address2,Address3,PostCode,Latitude,Longitude,City,Telephone,Email,Website,Twitter,Facebook,Active,DateAdded,DateUpdated")] Organisation organisation)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.Organisations.Add(organisation);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }

            ViewBag.CityId = new SelectList(_unitOfWork.Cities.GetCities(), "CityId", "CityName", organisation.CityId);
            return View(organisation);
        }

        // GET: Organisations/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var organisation = _unitOfWork.Organisations.GetOrganisation(id);
            if (organisation == null)
            {
                return HttpNotFound();
            }
            ViewBag.CityId = new SelectList(_unitOfWork.Cities.GetCities(), "CityId", "CityName", organisation.CityId);
            var organisationVM = Mapper.Map<OrganisationVM>(organisation);
            return View(organisationVM);
        }

        // POST: Organisations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OrganisationId,CityId,Name,Type,Title,SubTitle,Description,Address1,Address2,Address3,PostCode,Latitude,Longitude,City,Telephone,Email,Website,Twitter,Facebook,Active,DateAdded,DateUpdated")] Organisation organisation)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.DbStateModified(organisation);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }
            ViewBag.CityId = new SelectList(_unitOfWork.Cities.GetCities(), "CityId", "CityName", organisation.CityId);
            return View(organisation);
        }

        // GET: Organisations/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var organisation = _unitOfWork.Organisations.GetOrganisation(id);
            if (organisation == null)
            {
                return HttpNotFound();
            }
            var organisationVM = Mapper.Map<OrganisationVM>(organisation);
            return View(organisationVM);
        }

        // POST: Organisations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var organisation = _unitOfWork.Organisations.GetOrganisation(id);
            try {
                _unitOfWork.Organisations.Remove(organisation);
                _unitOfWork.Complete();
            }
            catch { }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
