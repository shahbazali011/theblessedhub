﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Models;
using BlessedHub.ViewModels;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using BlessedHub.Core.RepositoriesInterfaces;

namespace BlessedHub.Controllers
{
    public class GiveHelpSubTypesController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public GiveHelpSubTypesController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: GiveHelpSubTypes
        public ActionResult Index()
        {
            var giveHelpSubTypes = _unitOfWork.givehelpSubTypeRepository.GetGivehelpSubTypes();       
            var vm = giveHelpSubTypes.Select(Mapper.Map<GiveHelpSubType, GiveHelpSubTypeVM>);
            return View(vm);
        }

        // GET: GiveHelpSubTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
          
            var giveHelpSubType = _unitOfWork.givehelpSubTypeRepository.GetGivehelpSubType(id);

            if (giveHelpSubType == null)
            {
                return HttpNotFound();
            }
            var giveHelpSubTypeVM= Mapper.Map<GiveHelpSubTypeVM>(giveHelpSubType);
            return View(giveHelpSubTypeVM);
        }

        // GET: GiveHelpSubTypes/Create
        public ActionResult Create()
        {
            ViewBag.GiveHelpTypesId = new SelectList(_unitOfWork.giveHelpTypeRepository.GetGiveHelpTypes(), "Id", "HelpType");
            return View();
        }

        // POST: GiveHelpSubTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,GiveHelpTypesId,HelpSubType,Description")] GiveHelpSubType giveHelpSubType)
        {
            if (ModelState.IsValid)
            {
                
                _unitOfWork.givehelpSubTypeRepository.Add(giveHelpSubType);
                _unitOfWork.Complete();

                return RedirectToAction("Index");
            }

            ViewBag.GiveHelpTypesId = new SelectList(_unitOfWork.giveHelpTypeRepository.GetGiveHelpTypes(), "Id", "HelpType", giveHelpSubType.GiveHelpTypesId);
            return View(giveHelpSubType);
        }

        // GET: GiveHelpSubTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        
           var giveHelpSubType= _unitOfWork.givehelpSubTypeRepository.GetGivehelpSubType(id);
            if (giveHelpSubType == null)
            {
                return HttpNotFound();
            }
            ViewBag.GiveHelpTypesId = new SelectList(_unitOfWork.giveHelpTypeRepository.GetGiveHelpTypes(), "Id", "HelpType", giveHelpSubType.GiveHelpTypesId);
            var giveHelpSubTypeVM = Mapper.Map<GiveHelpSubTypeVM>(giveHelpSubType);
            return View(giveHelpSubTypeVM);
        }

        // POST: GiveHelpSubTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,GiveHelpTypesId,HelpSubType,Description")] GiveHelpSubType giveHelpSubType)
        {
            if (ModelState.IsValid)
            {
            
                _unitOfWork.DbStateModified(giveHelpSubType);
                _unitOfWork.Complete();
                return RedirectToAction("Index");
            }
            ViewBag.GiveHelpTypesId = new SelectList(_unitOfWork.giveHelpTypeRepository.GetGiveHelpTypes(), "Id", "HelpType", giveHelpSubType.GiveHelpTypesId);
            return View(giveHelpSubType);
        }

        // GET: GiveHelpSubTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
         
            var giveHelpSubType = _unitOfWork.givehelpSubTypeRepository.GetGivehelpSubType(id);
            if (giveHelpSubType == null)
            {
                return HttpNotFound();
            }
            var giveHelpSubTypeVM = Mapper.Map<GiveHelpSubTypeVM>(giveHelpSubType);
            return View(giveHelpSubTypeVM);
        }

        // POST: GiveHelpSubTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var giveHelpSubType = _unitOfWork.givehelpSubTypeRepository.GetGivehelpSubType(id);
            try
            {
                _unitOfWork.givehelpSubTypeRepository.Remove(giveHelpSubType);
               _unitOfWork.Complete();
            }
            catch { }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
