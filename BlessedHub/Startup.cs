﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BlessedHub.Startup))]
namespace BlessedHub
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
