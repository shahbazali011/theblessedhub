/****** Object:  Database [BlessedHub]    Script Date: 12/1/2019 14:23:08 ******/
CREATE DATABASE [BlessedHub]  (EDITION = 'GeneralPurpose', SERVICE_OBJECTIVE = 'GP_Gen5_2', MAXSIZE = 32 GB) WITH CATALOG_COLLATION = SQL_Latin1_General_CP1_CI_AS;
GO
ALTER DATABASE [BlessedHub] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [BlessedHub] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [BlessedHub] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [BlessedHub] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [BlessedHub] SET ARITHABORT OFF 
GO
ALTER DATABASE [BlessedHub] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [BlessedHub] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [BlessedHub] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [BlessedHub] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [BlessedHub] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [BlessedHub] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [BlessedHub] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [BlessedHub] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [BlessedHub] SET ALLOW_SNAPSHOT_ISOLATION ON 
GO
ALTER DATABASE [BlessedHub] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [BlessedHub] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [BlessedHub] SET  MULTI_USER 
GO
ALTER DATABASE [BlessedHub] SET ENCRYPTION ON
GO
ALTER DATABASE [BlessedHub] SET QUERY_STORE = ON
GO
ALTER DATABASE [BlessedHub] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 100, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO)
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 12/1/2019 14:23:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 12/1/2019 14:23:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 12/1/2019 14:23:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 12/1/2019 14:23:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 12/1/2019 14:23:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 12/1/2019 14:23:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BleassedHubTeams]    Script Date: 12/1/2019 14:23:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BleassedHubTeams](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TeamTitle] [varchar](50) NULL,
 CONSTRAINT [PK_BleassedHubTeams] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BlessedHubMembers]    Script Date: 12/1/2019 14:23:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BlessedHubMembers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[JobTitle] [varchar](50) NULL,
	[JobDescription] [varchar](50) NULL,
	[TeamId] [int] NOT NULL,
	[Mobiel] [varchar](50) NULL,
	[Phone] [varchar](50) NULL,
	[Role] [varchar](50) NULL,
	[Acievement] [varchar](50) NULL,
	[Address] [varchar](50) NULL,
	[Postcode] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[RoleId] [int] NULL,
	[Active] [bit] NULL,
	[DateAdded] [date] NULL,
	[DateUpdated] [nchar](10) NULL,
	[PictureUrl] [varchar](100) NULL,
 CONSTRAINT [PK_BlessedHubMembers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cities]    Script Date: 12/1/2019 14:23:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cities](
	[CityId] [int] IDENTITY(1,1) NOT NULL,
	[CityName] [varchar](500) NULL,
	[DateAdded] [date] NULL,
	[Active] [bit] NULL,
	[DateUpdated] [date] NULL,
 CONSTRAINT [PK_Cities] PRIMARY KEY CLUSTERED 
(
	[CityId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ContactUs]    Script Date: 12/1/2019 14:23:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContactUs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[Phone] [nvarchar](50) NULL,
	[Message] [varchar](max) NULL,
 CONSTRAINT [PK_ContactUs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EventTypes]    Script Date: 12/1/2019 14:23:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EventTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Type] [varchar](500) NULL,
	[EventTitle] [varchar](500) NULL,
	[Date] [date] NULL,
	[StartTime] [varchar](50) NULL,
	[EndTime] [varchar](50) NULL,
	[Venue] [varchar](500) NULL,
	[Entrance] [varchar](500) NULL,
	[Price] [varchar](50) NULL,
	[TickeetsLink] [varchar](500) NULL,
	[Frequency] [varchar](500) NULL,
	[FacilitiesAvailable] [varchar](500) NULL,
	[FutherDetail] [varchar](500) NULL,
	[Website] [varchar](500) NULL,
	[Email] [varchar](50) NULL,
	[SocialMedia] [varchar](500) NULL,
	[PicutreURL] [varchar](100) NULL,
 CONSTRAINT [PK_EventTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GiveHelpSubTypes]    Script Date: 12/1/2019 14:23:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GiveHelpSubTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GiveHelpTypesId] [int] NOT NULL,
	[HelpSubType] [varchar](500) NULL,
	[Description] [varchar](500) NULL,
 CONSTRAINT [PK_GiveHelpSubTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GiveHelpToOrganisaiton]    Script Date: 12/1/2019 14:23:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GiveHelpToOrganisaiton](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GiveHelpSubTypesId] [int] NOT NULL,
	[OrganisationId] [int] NOT NULL,
	[Description] [varchar](500) NULL,
	[GiveHelpTypesId] [int] NOT NULL,
	[DateAdded] [date] NULL,
	[Active] [bit] NULL,
	[DateUpdated] [date] NULL,
 CONSTRAINT [PK_GiveHelpToOrganisaiton] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GiveHelpTypes]    Script Date: 12/1/2019 14:23:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GiveHelpTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HelpType] [varchar](500) NULL,
	[Description] [varchar](max) NULL,
 CONSTRAINT [PK_GiveHelpTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ICanHelp]    Script Date: 12/1/2019 14:23:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ICanHelp](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GiveHelpToOrganisaitonId] [int] NOT NULL,
	[Email] [varchar](50) NULL,
	[Message] [varchar](max) NULL,
	[Descrption] [varchar](500) NULL,
 CONSTRAINT [PK_ICanHelp] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MasjidDetails]    Script Date: 12/1/2019 14:23:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MasjidDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MasjidName] [varchar](500) NULL,
	[Address] [varchar](500) NULL,
	[PostCode] [varchar](50) NULL,
	[Phone] [varchar](50) NULL,
	[ImamName] [varchar](50) NULL,
	[ImamMobile] [varchar](50) NULL,
 CONSTRAINT [PK_MasjidDetails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[News]    Script Date: 12/1/2019 14:23:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[News](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NewsTitle] [varchar](500) NULL,
	[NewsDetail] [varchar](max) NULL,
	[MoreDetails] [varchar](500) NULL,
	[PostedDate] [varchar](50) NULL,
	[PostedBy] [varchar](50) NULL,
	[NewsTypeId] [int] NOT NULL,
	[NewsSubTypeId] [int] NOT NULL,
	[FacebookLink] [varchar](500) NULL,
	[TwitterLink] [varchar](500) NULL,
	[Tagged] [varchar](500) NULL,
	[PictureURL] [varchar](500) NULL,
	[Active] [bit] NULL,
	[DateAdded] [date] NULL,
	[DateUpdated] [date] NULL,
 CONSTRAINT [PK_News] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NewsSubTypes]    Script Date: 12/1/2019 14:23:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsSubTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NewsTypeId] [int] NOT NULL,
	[NewsSubType] [varchar](50) NULL,
	[Description] [varchar](50) NULL,
 CONSTRAINT [PK_NewsSubTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NewsTypes]    Script Date: 12/1/2019 14:23:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NewsType] [varchar](50) NULL,
	[Description] [varchar](50) NULL,
 CONSTRAINT [PK_NewsTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrganisaitonServicesTypes]    Script Date: 12/1/2019 14:23:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrganisaitonServicesTypes](
	[OrganisaitonServicesTypesId] [int] IDENTITY(1,1) NOT NULL,
	[ServiceTypeId] [int] NOT NULL,
	[OrganisationId] [int] NOT NULL,
	[SuitableFor] [varchar](50) NULL,
	[Description] [varchar](max) NULL,
	[ServiceId] [int] NOT NULL,
	[DateAdded] [date] NULL,
	[Active] [bit] NULL,
	[DateUpdated] [date] NULL,
 CONSTRAINT [PK_OrganisaitonServicesTypes] PRIMARY KEY CLUSTERED 
(
	[OrganisaitonServicesTypesId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Organisation]    Script Date: 12/1/2019 14:23:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Organisation](
	[OrganisationId] [int] IDENTITY(1,1) NOT NULL,
	[CityId] [int] NOT NULL,
	[Name] [varchar](500) NULL,
	[Type] [varchar](500) NULL,
	[Title] [varchar](500) NULL,
	[SubTitle] [varchar](500) NULL,
	[Description] [varchar](max) NULL,
	[Address1] [varchar](500) NULL,
	[Address2] [varchar](500) NULL,
	[Address3] [varchar](500) NULL,
	[PostCode] [varchar](500) NULL,
	[Latitude] [decimal](18, 6) NULL,
	[Longitude] [decimal](18, 6) NULL,
	[City] [varchar](500) NULL,
	[Telephone] [varchar](500) NULL,
	[Email] [varchar](500) NULL,
	[Website] [varchar](500) NULL,
	[Twitter] [varchar](500) NULL,
	[Facebook] [varchar](500) NULL,
	[Active] [bit] NULL,
	[DateAdded] [date] NULL,
	[DateUpdated] [date] NULL,
 CONSTRAINT [PK_Organisation] PRIMARY KEY CLUSTERED 
(
	[OrganisationId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrganisationServices]    Script Date: 12/1/2019 14:23:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrganisationServices](
	[OrganisationServicesId] [int] IDENTITY(1,1) NOT NULL,
	[OrganisationId] [int] NOT NULL,
	[Description] [varchar](max) NULL,
	[ServiceId] [int] NOT NULL,
	[DateAdded] [date] NULL,
	[Active] [bit] NULL,
	[DateUpdated] [date] NULL,
 CONSTRAINT [PK_OrganisationServices] PRIMARY KEY CLUSTERED 
(
	[OrganisationServicesId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrganisationWorkingDays]    Script Date: 12/1/2019 14:23:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrganisationWorkingDays](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DayId] [int] NULL,
	[OrganisationId] [int] NULL,
	[WorkingDay] [bit] NULL,
	[StartTime] [varchar](50) NULL,
	[EndTime] [varchar](50) NULL,
 CONSTRAINT [PK_OrganisationWorkingDays] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PrayerTimings]    Script Date: 12/1/2019 14:23:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PrayerTimings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MasjidId] [int] NOT NULL,
	[CalendarPicURL] [varchar](50) NULL,
	[DateAdded] [date] NULL,
	[DateUpdated] [date] NULL,
 CONSTRAINT [PK_PrayerTimings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Services]    Script Date: 12/1/2019 14:23:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Services](
	[ServiceId] [int] IDENTITY(1,1) NOT NULL,
	[ServiceName] [varchar](500) NULL,
	[Description] [varchar](500) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_Services] PRIMARY KEY CLUSTERED 
(
	[ServiceId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ServicesTypes]    Script Date: 12/1/2019 14:23:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServicesTypes](
	[ServiceTypeId] [int] IDENTITY(1,1) NOT NULL,
	[ServiceType] [varchar](500) NULL,
	[ServiceId] [int] NOT NULL,
	[Description] [varchar](500) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_ServicesTypes] PRIMARY KEY CLUSTERED 
(
	[ServiceTypeId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ServiceTypeWorkingDays]    Script Date: 12/1/2019 14:23:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServiceTypeWorkingDays](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DayId] [int] NOT NULL,
	[ServiceId] [int] NOT NULL,
	[ServiceTypeId] [int] NOT NULL,
	[OrganisationId] [int] NOT NULL,
	[WorkingDay] [bit] NULL,
	[StartTime] [varchar](50) NULL,
	[EndTime] [varchar](50) NULL,
 CONSTRAINT [PK_ServiceTypeWorkingDays] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Volunteers]    Script Date: 12/1/2019 14:23:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Volunteers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[EmailAddress] [varchar](50) NULL,
	[TelephoneNumber] [varchar](50) NULL,
	[CityId] [int] NOT NULL,
	[PostCode] [varchar](50) NULL,
	[Skills] [varchar](500) NULL,
	[Availablity] [varchar](500) NULL,
	[Resources] [varchar](500) NULL,
	[DateAdded] [date] NULL,
	[Active] [bit] NULL,
	[DateUpdated] [date] NULL,
 CONSTRAINT [PK_Volunteers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WeekDays]    Script Date: 12/1/2019 14:23:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WeekDays](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Days] [varchar](50) NULL,
 CONSTRAINT [PK_WeekDays] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [RoleNameIndex]    Script Date: 12/1/2019 14:23:08 ******/
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
(
	[Name] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_UserId]    Script Date: 12/1/2019 14:23:08 ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserClaims]
(
	[UserId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_UserId]    Script Date: 12/1/2019 14:23:08 ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserLogins]
(
	[UserId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_RoleId]    Script Date: 12/1/2019 14:23:08 ******/
CREATE NONCLUSTERED INDEX [IX_RoleId] ON [dbo].[AspNetUserRoles]
(
	[RoleId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_UserId]    Script Date: 12/1/2019 14:23:08 ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserRoles]
(
	[UserId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UserNameIndex]    Script Date: 12/1/2019 14:23:08 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
(
	[UserName] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GiveHelpToOrganisaiton] ADD  CONSTRAINT [DF_GiveHelpToOrganisaiton_GiveHelpTypesId]  DEFAULT ((1)) FOR [GiveHelpTypesId]
GO
ALTER TABLE [dbo].[OrganisaitonServicesTypes] ADD  CONSTRAINT [DF_OrganisaitonServicesTypes_ServiceId]  DEFAULT ((1)) FOR [ServiceId]
GO
ALTER TABLE [dbo].[OrganisationServices] ADD  CONSTRAINT [DF_OrganisationServices_ServiceId]  DEFAULT ((1)) FOR [ServiceId]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[BlessedHubMembers]  WITH CHECK ADD  CONSTRAINT [FK_BlessedHubMembers_BleassedHubTeams] FOREIGN KEY([TeamId])
REFERENCES [dbo].[BleassedHubTeams] ([Id])
GO
ALTER TABLE [dbo].[BlessedHubMembers] CHECK CONSTRAINT [FK_BlessedHubMembers_BleassedHubTeams]
GO
ALTER TABLE [dbo].[GiveHelpSubTypes]  WITH CHECK ADD  CONSTRAINT [FK_GiveHelpSubTypes_GiveHelpTypes] FOREIGN KEY([GiveHelpTypesId])
REFERENCES [dbo].[GiveHelpTypes] ([Id])
GO
ALTER TABLE [dbo].[GiveHelpSubTypes] CHECK CONSTRAINT [FK_GiveHelpSubTypes_GiveHelpTypes]
GO
ALTER TABLE [dbo].[GiveHelpToOrganisaiton]  WITH CHECK ADD  CONSTRAINT [FK_GiveHelpToOrganisaiton_GiveHelpSubTypes] FOREIGN KEY([GiveHelpSubTypesId])
REFERENCES [dbo].[GiveHelpSubTypes] ([Id])
GO
ALTER TABLE [dbo].[GiveHelpToOrganisaiton] CHECK CONSTRAINT [FK_GiveHelpToOrganisaiton_GiveHelpSubTypes]
GO
ALTER TABLE [dbo].[GiveHelpToOrganisaiton]  WITH CHECK ADD  CONSTRAINT [FK_GiveHelpToOrganisaiton_GiveHelpTypes] FOREIGN KEY([GiveHelpTypesId])
REFERENCES [dbo].[GiveHelpTypes] ([Id])
GO
ALTER TABLE [dbo].[GiveHelpToOrganisaiton] CHECK CONSTRAINT [FK_GiveHelpToOrganisaiton_GiveHelpTypes]
GO
ALTER TABLE [dbo].[GiveHelpToOrganisaiton]  WITH CHECK ADD  CONSTRAINT [FK_GiveHelpToOrganisaiton_Organisation] FOREIGN KEY([OrganisationId])
REFERENCES [dbo].[Organisation] ([OrganisationId])
GO
ALTER TABLE [dbo].[GiveHelpToOrganisaiton] CHECK CONSTRAINT [FK_GiveHelpToOrganisaiton_Organisation]
GO
ALTER TABLE [dbo].[ICanHelp]  WITH CHECK ADD  CONSTRAINT [FK_ICanHelp_GiveHelpToOrganisaiton] FOREIGN KEY([GiveHelpToOrganisaitonId])
REFERENCES [dbo].[GiveHelpToOrganisaiton] ([Id])
GO
ALTER TABLE [dbo].[ICanHelp] CHECK CONSTRAINT [FK_ICanHelp_GiveHelpToOrganisaiton]
GO
ALTER TABLE [dbo].[News]  WITH CHECK ADD  CONSTRAINT [FK_News_NewsSubTypes] FOREIGN KEY([NewsSubTypeId])
REFERENCES [dbo].[NewsSubTypes] ([Id])
GO
ALTER TABLE [dbo].[News] CHECK CONSTRAINT [FK_News_NewsSubTypes]
GO
ALTER TABLE [dbo].[News]  WITH CHECK ADD  CONSTRAINT [FK_News_NewsTypes] FOREIGN KEY([NewsTypeId])
REFERENCES [dbo].[NewsTypes] ([Id])
GO
ALTER TABLE [dbo].[News] CHECK CONSTRAINT [FK_News_NewsTypes]
GO
ALTER TABLE [dbo].[NewsSubTypes]  WITH CHECK ADD  CONSTRAINT [FK_NewsSubTypes_NewsTypes] FOREIGN KEY([NewsTypeId])
REFERENCES [dbo].[NewsTypes] ([Id])
GO
ALTER TABLE [dbo].[NewsSubTypes] CHECK CONSTRAINT [FK_NewsSubTypes_NewsTypes]
GO
ALTER TABLE [dbo].[OrganisaitonServicesTypes]  WITH CHECK ADD  CONSTRAINT [FK_OrganisaitonServicesTypes_Organisation] FOREIGN KEY([OrganisationId])
REFERENCES [dbo].[Organisation] ([OrganisationId])
GO
ALTER TABLE [dbo].[OrganisaitonServicesTypes] CHECK CONSTRAINT [FK_OrganisaitonServicesTypes_Organisation]
GO
ALTER TABLE [dbo].[OrganisaitonServicesTypes]  WITH CHECK ADD  CONSTRAINT [FK_OrganisaitonServicesTypes_Services] FOREIGN KEY([ServiceId])
REFERENCES [dbo].[Services] ([ServiceId])
GO
ALTER TABLE [dbo].[OrganisaitonServicesTypes] CHECK CONSTRAINT [FK_OrganisaitonServicesTypes_Services]
GO
ALTER TABLE [dbo].[OrganisaitonServicesTypes]  WITH CHECK ADD  CONSTRAINT [FK_OrganisaitonServicesTypes_ServicesTypes] FOREIGN KEY([ServiceTypeId])
REFERENCES [dbo].[ServicesTypes] ([ServiceTypeId])
GO
ALTER TABLE [dbo].[OrganisaitonServicesTypes] CHECK CONSTRAINT [FK_OrganisaitonServicesTypes_ServicesTypes]
GO
ALTER TABLE [dbo].[Organisation]  WITH CHECK ADD  CONSTRAINT [FK_Organisation_Organisation] FOREIGN KEY([CityId])
REFERENCES [dbo].[Cities] ([CityId])
GO
ALTER TABLE [dbo].[Organisation] CHECK CONSTRAINT [FK_Organisation_Organisation]
GO
ALTER TABLE [dbo].[OrganisationServices]  WITH CHECK ADD  CONSTRAINT [FK_OrganisationServices_Organisation] FOREIGN KEY([OrganisationId])
REFERENCES [dbo].[Organisation] ([OrganisationId])
GO
ALTER TABLE [dbo].[OrganisationServices] CHECK CONSTRAINT [FK_OrganisationServices_Organisation]
GO
ALTER TABLE [dbo].[OrganisationServices]  WITH CHECK ADD  CONSTRAINT [FK_OrganisationServices_Services] FOREIGN KEY([ServiceId])
REFERENCES [dbo].[Services] ([ServiceId])
GO
ALTER TABLE [dbo].[OrganisationServices] CHECK CONSTRAINT [FK_OrganisationServices_Services]
GO
ALTER TABLE [dbo].[OrganisationWorkingDays]  WITH CHECK ADD  CONSTRAINT [FK_OrganisationWorkingDays_Organisation] FOREIGN KEY([OrganisationId])
REFERENCES [dbo].[Organisation] ([OrganisationId])
GO
ALTER TABLE [dbo].[OrganisationWorkingDays] CHECK CONSTRAINT [FK_OrganisationWorkingDays_Organisation]
GO
ALTER TABLE [dbo].[OrganisationWorkingDays]  WITH CHECK ADD  CONSTRAINT [FK_OrganisationWorkingDays_WeekDays] FOREIGN KEY([DayId])
REFERENCES [dbo].[WeekDays] ([Id])
GO
ALTER TABLE [dbo].[OrganisationWorkingDays] CHECK CONSTRAINT [FK_OrganisationWorkingDays_WeekDays]
GO
ALTER TABLE [dbo].[PrayerTimings]  WITH CHECK ADD  CONSTRAINT [FK_PrayerTimings_MasjidDetails] FOREIGN KEY([MasjidId])
REFERENCES [dbo].[MasjidDetails] ([Id])
GO
ALTER TABLE [dbo].[PrayerTimings] CHECK CONSTRAINT [FK_PrayerTimings_MasjidDetails]
GO
ALTER TABLE [dbo].[ServicesTypes]  WITH CHECK ADD  CONSTRAINT [FK_ServicesTypes_Services] FOREIGN KEY([ServiceId])
REFERENCES [dbo].[Services] ([ServiceId])
GO
ALTER TABLE [dbo].[ServicesTypes] CHECK CONSTRAINT [FK_ServicesTypes_Services]
GO
ALTER TABLE [dbo].[ServiceTypeWorkingDays]  WITH CHECK ADD  CONSTRAINT [FK_ServiceTypeWorkingDays_Organisation] FOREIGN KEY([OrganisationId])
REFERENCES [dbo].[Organisation] ([OrganisationId])
GO
ALTER TABLE [dbo].[ServiceTypeWorkingDays] CHECK CONSTRAINT [FK_ServiceTypeWorkingDays_Organisation]
GO
ALTER TABLE [dbo].[ServiceTypeWorkingDays]  WITH CHECK ADD  CONSTRAINT [FK_ServiceTypeWorkingDays_Services] FOREIGN KEY([ServiceId])
REFERENCES [dbo].[Services] ([ServiceId])
GO
ALTER TABLE [dbo].[ServiceTypeWorkingDays] CHECK CONSTRAINT [FK_ServiceTypeWorkingDays_Services]
GO
ALTER TABLE [dbo].[ServiceTypeWorkingDays]  WITH CHECK ADD  CONSTRAINT [FK_ServiceTypeWorkingDays_ServicesTypes] FOREIGN KEY([ServiceTypeId])
REFERENCES [dbo].[ServicesTypes] ([ServiceTypeId])
GO
ALTER TABLE [dbo].[ServiceTypeWorkingDays] CHECK CONSTRAINT [FK_ServiceTypeWorkingDays_ServicesTypes]
GO
ALTER TABLE [dbo].[ServiceTypeWorkingDays]  WITH CHECK ADD  CONSTRAINT [FK_ServiceTypeWorkingDays_WeekDays] FOREIGN KEY([DayId])
REFERENCES [dbo].[WeekDays] ([Id])
GO
ALTER TABLE [dbo].[ServiceTypeWorkingDays] CHECK CONSTRAINT [FK_ServiceTypeWorkingDays_WeekDays]
GO
ALTER TABLE [dbo].[Volunteers]  WITH CHECK ADD  CONSTRAINT [FK_Volunteers_Cities] FOREIGN KEY([CityId])
REFERENCES [dbo].[Cities] ([CityId])
GO
ALTER TABLE [dbo].[Volunteers] CHECK CONSTRAINT [FK_Volunteers_Cities]
GO
ALTER DATABASE [BlessedHub] SET  READ_WRITE 
GO
