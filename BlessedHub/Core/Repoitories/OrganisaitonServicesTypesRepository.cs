﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BlessedHub.Models;
using System.Data.Entity;
using BlessedHub.Core.RepositoriesInterfaces;

namespace BlessedHub.Presistence.Repositories
{
    public class OrganisaitonServicesTypesRepository : IOrganisaitonServicesTypesRepository
    {
        private readonly TheBlessedHubEntities _context;

        public OrganisaitonServicesTypesRepository(TheBlessedHubEntities context)
        {
            _context = context;
        }

        public void Add(OrganisaitonServicesType OrganisaitonServicesType)
        {
            var record = _context.OrganisaitonServicesTypes.Where(s => s.OrganisationId == OrganisaitonServicesType.OrganisationId 
            && s.ServiceId == OrganisaitonServicesType.ServiceId 
            && s.ServiceTypeId== OrganisaitonServicesType.ServiceTypeId);
            if (record.Count() == 0)
            {
                _context.OrganisaitonServicesTypes.Add(OrganisaitonServicesType);
            }
        }

        public void Remove(OrganisaitonServicesType OrganisaitonServicesType)
        {
            _context.OrganisaitonServicesTypes.Remove(OrganisaitonServicesType);
        }

        public OrganisaitonServicesType GetOrganisaitonServicesType(int? id)
        {
           return _context.OrganisaitonServicesTypes.Find(id);
        }

        public IEnumerable<OrganisaitonServicesType> GetOrganisaitonServicesTypes()
        {
           return _context.OrganisaitonServicesTypes.Include(o => o.Organisation).Include(o => o.Service).Include(o => o.ServicesType);
        }

    

        public IEnumerable<OrganisaitonServicesType> OrganisaitonServicesTypes()
        {
            var organisaitonServicesTypes = _context.OrganisaitonServicesTypes.Include(o => o.Organisation).Include(o => o.ServicesType);
            return organisaitonServicesTypes;
        }

        public IEnumerable<OrganisaitonServicesType> OrganisaitonServicesTypesAll(int? OrganisationId, int? ServiceId)
        {
            var organisaitonServicesTypes = _context.OrganisaitonServicesTypes
              .Where(or => or.Organisation.OrganisationId == OrganisationId && or.ServicesType.ServiceId == ServiceId)
              .Include(o => o.Organisation).Include(o => o.ServicesType).Include(o => o.Service);
            return organisaitonServicesTypes;
        }

        public IEnumerable<OrganisaitonServicesType> PartialServicesTypesCard(int? OrganisationId, int? OrganisationServicesId)
        {
            var organisaitonServicesTypes = _context.OrganisaitonServicesTypes
               .Where(or => or.Organisation.OrganisationId == OrganisationId)
               .Include(o => o.Organisation).Include(o => o.ServicesType).Include(o => o.Service);
            return organisaitonServicesTypes;
        }

       
    }
}