﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BlessedHub.Core.RepositoriesInterfaces;
using BlessedHub.Models;
using System.Data.Entity;
namespace BlessedHub.Presistence.Repositories
{
    public class ServiceTypeRepository : IServiceTypeRepository
    {
        private readonly TheBlessedHubEntities _context;
        public ServiceTypeRepository(TheBlessedHubEntities context)
        {
            _context = context;
        }

        public void Add(ServicesType ServicesType)
        {
            var record = _context.ServicesTypes.Where(s => s.ServiceType == ServicesType.ServiceType);
            if (record.Count() == 0)
            {
                _context.ServicesTypes.Add(ServicesType);
            }
        }

        public ServicesType GetServicesType(int? id)
        {
            return _context.ServicesTypes.Find(id);
        }

        public IEnumerable<ServicesType> GetServicesTypes()
        {
            var servicesTypes = _context.ServicesTypes.Include(s => s.Service);
            return servicesTypes;
        }

        public IEnumerable<ServicesType> GetServicesTypeSearch(string search)
        {
            return _context.ServicesTypes.Where(s=>s.ServiceType.StartsWith(search));
        }

        public void Remove(ServicesType ServicesType)
        {
            _context.ServicesTypes.Remove(ServicesType);
        }
    }
}