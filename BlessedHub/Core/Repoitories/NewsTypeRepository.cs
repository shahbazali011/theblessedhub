﻿using BlessedHub.Core.RepositoriesInterfaces;
using BlessedHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
namespace BlessedHub.Core.Repoitories
{
    public class NewsTypeRepository : INewsTypeRepository
    {
        private readonly TheBlessedHubEntities _context;
        public NewsTypeRepository(TheBlessedHubEntities context)
        {
            _context = context;
        }
        public void Add(NewsType NewsType)
        {
            var record = _context.NewsTypes.Where(s => s.NewsType1 == NewsType.NewsType1);
            if (record.Count() == 0)
            {
                _context.NewsTypes.Add(NewsType);
            }
        }
         
        public NewsType GetNewsType(int? id)
        {
            return _context.NewsTypes.Find(id);
        }

        public IEnumerable<NewsType> GetNewsTypes()
        {
            //var query = _context.NewsTypes.Join(_context.NewsTypes, r => r.Id, r=, (r, p) => new { r.FirstName, r.LastName, p.DepartmentName });
            return _context.NewsTypes;
        }

        public void Remove(NewsType NewsType)
        {
            _context.NewsTypes.Remove(NewsType);
        }
    }
}