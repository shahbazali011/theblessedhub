﻿using BlessedHub.Core.RepositoriesInterfaces;
using BlessedHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlessedHub.Core.Repoitories
{
    public class BlessedHubTeamRepository : IBlessedHubTeam
    {
        private readonly TheBlessedHubEntities _context;
        public BlessedHubTeamRepository(TheBlessedHubEntities context)
        {
            _context = context;
        }
        public void Add(BleassedHubTeam BleassedHubTeam)
        {
            _context.BleassedHubTeams.Add(BleassedHubTeam);
        }

        public BleassedHubTeam GetBleassedHubTeam(int? id)
        {
            return _context.BleassedHubTeams.Find(id);
        }

        public IEnumerable<BleassedHubTeam> GetBleassedHubTeams()
        {
            return _context.BleassedHubTeams;
        }

        public void Remove(BleassedHubTeam BleassedHubTeam)
        {
            _context.BleassedHubTeams.Remove(BleassedHubTeam);
        }
    }
}