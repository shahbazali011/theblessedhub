﻿using BlessedHub.Core.RepositoriesInterfaces;
using BlessedHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlessedHub.Core.Repoitories
{
    public class CountAll : ICountAll
    {
        private readonly TheBlessedHubEntities _context;
        public CountAll(TheBlessedHubEntities context)
        {
            _context = context;
        }
        public int CountNeeds()
        {
            return _context.GiveHelpToOrganisaitons.Count();
        }

        public int CountOrganisations()
        {
            return _context.Organisations.Count();
        }

        public int CountServices()
        {
           return _context.OrganisaitonServicesTypes.Count();
        }
    }
}