﻿using BlessedHub.Core.RepositoriesInterfaces;
using BlessedHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
namespace BlessedHub.Core.Repoitories
{
    public class ICanHelpRepository : IICanHelpRepository
    {
        private readonly TheBlessedHubEntities _context;
        public ICanHelpRepository(TheBlessedHubEntities context)
        {
            _context = context;
        }
        public void Add(ICanHelp ICanHelp)
        {
            _context.ICanHelps.Add(ICanHelp);
        }

        public ICanHelp GetICanHelp(int? id)
        {
            return _context.ICanHelps.Find(id);
        }

        public IEnumerable<ICanHelp> GetICanHelps()
        {
            return _context.ICanHelps.Include(i => i.GiveHelpToOrganisaiton);
        }

        public void Remove(ICanHelp ICanHelp)
        {
            _context.ICanHelps.Remove(ICanHelp);
        }
    }
}