﻿using BlessedHub.Core.RepositoriesInterfaces;
using BlessedHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlessedHub.Core.Repoitories
{
    public class WeekDayRepository : IWeekDayRepository
    {
        private readonly TheBlessedHubEntities _context;
        public WeekDayRepository(TheBlessedHubEntities context)
        {
            _context = context;
        }
        public void Add(WeekDay WeekDay)
        {
            _context.WeekDays.Add(WeekDay);
        }

        public WeekDay GetWeekDay(int? id)
        {
            return _context.WeekDays.Find(id);
        }

        public IEnumerable<WeekDay> GetWeekDays()
        {
            return _context.WeekDays;
        }

        public void Remove(WeekDay WeekDay)
        {
            _context.WeekDays.Remove(WeekDay);
        }
    }
}