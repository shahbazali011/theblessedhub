﻿using BlessedHub.Core.RepositoriesInterfaces;
using BlessedHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace BlessedHub.Core.Repoitories
{
    public class GiveHelpToOrganisaitonRepository : IGiveHelpToOrganisaitonRepository
    {
        private readonly TheBlessedHubEntities _context;
        public GiveHelpToOrganisaitonRepository(TheBlessedHubEntities context)
        {
            _context = context;
        }
        public void Add(GiveHelpToOrganisaiton GiveHelpToOrganisaiton)
        {
            var record = _context.GiveHelpToOrganisaitons.Where(s => s.OrganisationId == GiveHelpToOrganisaiton.OrganisationId
            && s.GiveHelpTypesId == GiveHelpToOrganisaiton.GiveHelpTypesId
            && s.GiveHelpSubTypesId== GiveHelpToOrganisaiton.GiveHelpSubTypesId);
            if (record.Count() == 0)
            {
                _context.GiveHelpToOrganisaitons.Add(GiveHelpToOrganisaiton);
            }
        }

        public IEnumerable<GiveHelpToOrganisaiton> GetGiveHelpToOrganisaitons()
        {
            return _context.GiveHelpToOrganisaitons.Include(g => g.GiveHelpSubType).Include(g => g.GiveHelpType).Include(g => g.Organisation);
        }

        public IEnumerable<GiveHelpToOrganisaiton> GetGiveHelpToOrganisaitons(int? id)
        {
            return  _context.GiveHelpToOrganisaitons.Where(ht => ht.GiveHelpType.Id == id).Include(g => g.GiveHelpSubType).Include(g => g.Organisation).Include(g => g.GiveHelpType);

        }

        public GiveHelpToOrganisaiton GiveHelpToOrganisaiton(int? id)
        {
            return _context.GiveHelpToOrganisaitons.Find(id);
        }

        public void Remove(GiveHelpToOrganisaiton GiveHelpToOrganisaiton)
        {
            _context.GiveHelpToOrganisaitons.Remove(GiveHelpToOrganisaiton);
        }
    }
}