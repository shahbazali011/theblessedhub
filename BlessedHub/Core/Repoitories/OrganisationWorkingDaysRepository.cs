﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Core.RepositoriesInterfaces;
using BlessedHub.Models;

namespace BlessedHub.Presistence.Repositories
{
    public class OrganisationWorkingDaysRepository :IOrganisationWorkingDaysRepository
    {
        private readonly TheBlessedHubEntities _context;
        public OrganisationWorkingDaysRepository(TheBlessedHubEntities context)
        {
            _context = context;
        }

        public void Add(OrganisationWorkingDay OrganisationWorkingDay)
        {
            var record = _context.OrganisationWorkingDays.Where(s => s.OrganisationId == OrganisationWorkingDay.OrganisationId && s.DayId== OrganisationWorkingDay.DayId);
            if (record.Count() == 0)
            {
                _context.OrganisationWorkingDays.Add(OrganisationWorkingDay);
            }
        }

        public IEnumerable<OrganisationWorkingDay> GetOrganisationWorkingDays()
        {
            var organisationWorkingDays = _context.OrganisationWorkingDays.Include(o => o.Organisation).Include(o => o.WeekDay);
            return (organisationWorkingDays);
        }

        public OrganisationWorkingDay GetWorkingDay(int? id)
        {
            return _context.OrganisationWorkingDays.Find(id);
        }

        public IEnumerable<OrganisationWorkingDay> OranisationTimeTable(int? id)
        {
            var organisationWorkingDays = _context.OrganisationWorkingDays.Where(o => o.OrganisationId == id).Include(o => o.Organisation).Include(o => o.WeekDay);
            return (organisationWorkingDays);
        }

        public void Remove(OrganisationWorkingDay OrganisationWorkingDay)
        {
            _context.OrganisationWorkingDays.Remove(OrganisationWorkingDay);
        }
    }
}