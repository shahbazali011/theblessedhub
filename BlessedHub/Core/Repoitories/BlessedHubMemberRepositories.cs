﻿using BlessedHub.Core.RepositoriesInterfaces;
using BlessedHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace BlessedHub.Core.Repoitories
{
    public class BlessedHubMemberRepositories : IBlessedHubMembers
    {
        private readonly TheBlessedHubEntities _context;
        public BlessedHubMemberRepositories(TheBlessedHubEntities context)
        {
            _context = context;
        }
        public void Add(BlessedHubMember BlessedHubMember)
        {
            _context.BlessedHubMembers.Add(BlessedHubMember);
        }

        public BlessedHubMember GetBlessedHubMember(int? id)
        {
            return _context.BlessedHubMembers.Find(id);
        }

        public IEnumerable<BlessedHubMember> GetBlessedHubMembers()
        {
            return _context.BlessedHubMembers.Include(b => b.BleassedHubTeam);
        }

        public IEnumerable<BlessedHubMember> GetBlessedHubMembers(int? id)
        {
            return _context.BlessedHubMembers.Where(team=>team.BleassedHubTeam.Id==id).Include(b => b.BleassedHubTeam);
        }

        public void Remove(BlessedHubMember BlessedHubMember)
        {
            _context.BlessedHubMembers.Remove(BlessedHubMember);
        }
    }
}