﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BlessedHub.Models;
using System.Data.Entity;
using BlessedHub.Core.RepositoriesInterfaces;

namespace BlessedHub.Presistence.Repositories
{
    public class ServiceTypeWorkingDaysDataRepository : IServiceTypeWorkingDaysDataRepository
    {
        private readonly TheBlessedHubEntities _context;
        public ServiceTypeWorkingDaysDataRepository(TheBlessedHubEntities context)
        {
            _context = context;
        }

        public void Add(ServiceTypeWorkingDay ServiceTypeWorkingDay)
        {
            var record = _context.ServiceTypeWorkingDays.Where(s => s.OrganisationId == ServiceTypeWorkingDay.OrganisationId 
            && s.DayId == ServiceTypeWorkingDay.DayId 
            && s.ServiceId== ServiceTypeWorkingDay.ServiceId
            && s.ServiceTypeId== ServiceTypeWorkingDay.ServiceTypeId);
            if (record.Count() == 0)
            {
                _context.ServiceTypeWorkingDays.Add(ServiceTypeWorkingDay);
            }
        }

        public IEnumerable<ServiceTypeWorkingDay> GetServiceTypeWorkingDays()
        {
            var serviceTypeWorkingDays = _context.ServiceTypeWorkingDays.Include(s => s.Service).Include(s => s.ServicesType).Include(s => s.WeekDay).Include(s => s.Organisation);
            return serviceTypeWorkingDays;
        }

        public IEnumerable<ServiceTypeWorkingDay> GetServiceTypeWorkingDays(int id)
        {
            var serviceTypeWorkingDays = _context.ServiceTypeWorkingDays.Where(s => s.ServiceId == id).Include(s => s.Service).Include(s => s.ServicesType).Include(s => s.WeekDay);
            return serviceTypeWorkingDays;
        }

        public ServiceTypeWorkingDay GetWorkingDay(int? id)
        {
            return _context.ServiceTypeWorkingDays.Find(id);
        }

        public IEnumerable<ServiceTypeWorkingDay> PartialAllServicesTypesDetails(int? id)
        {
            //var test = _context.ServiceTypeWorkingDays.Where(s => s.ServiceId == id).Include(s => s.Service).Include(s => s.ServicesType).Include(s => s.WeekDay).ToList();
            var serviceTypeWorkingDays = _context.ServiceTypeWorkingDays.Where(s => s.ServiceId == id).Include(s => s.Service).Include(s => s.ServicesType).Include(s => s.WeekDay);
            return serviceTypeWorkingDays;
        }

        public IEnumerable<ServiceTypeWorkingDay> PartialMapOrganisation(int? id)
        {
            var serviceTypeWorkingDays = _context.ServiceTypeWorkingDays.Where(s => s.ServiceId == id).Include(s => s.Service).Include(s => s.ServicesType).Include(s => s.WeekDay);
            return serviceTypeWorkingDays;
        }

        public IEnumerable<ServiceTypeWorkingDay> PartialServiceTypeTimeTable(int? ServiceTypeId, int? ServiceId, int? OrganisationId)
        {
            var serviceTypeWorkingDays = _context.ServiceTypeWorkingDays
             .Where(st => st.ServiceTypeId == ServiceTypeId && st.ServiceId == ServiceId && st.OrganisationId == OrganisationId)
             .Include(s => s.Service).Include(s => s.ServicesType).Include(s => s.WeekDay).Include(s => s.Organisation);
            return serviceTypeWorkingDays;
        }

        public void Remove(ServiceTypeWorkingDay ServiceTypeWorkingDay)
        {
            _context.ServiceTypeWorkingDays.Remove(ServiceTypeWorkingDay);
        }
    }
}