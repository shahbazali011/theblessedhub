﻿using BlessedHub.Core.RepositoriesInterfaces;
using BlessedHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
namespace BlessedHub.Core.Repoitories
{
    public class GivehelpSubTypeRepository : IGivehelpSubTypeRepository
    {
        private readonly TheBlessedHubEntities _context;
        public GivehelpSubTypeRepository(TheBlessedHubEntities context)
        {
            _context = context;
        }
        public void Add(GiveHelpSubType GivehelpSubType)
        {
            var record = _context.GiveHelpSubTypes.Where(s => s.HelpSubType == GivehelpSubType.HelpSubType);
            if (record.Count() == 0)
            {
                _context.GiveHelpSubTypes.Add(GivehelpSubType);
            }
        }

        public GiveHelpSubType GetGivehelpSubType(int? id)
        {
            return _context.GiveHelpSubTypes.Find(id);
        }

        public IEnumerable<GiveHelpSubType> GetGivehelpSubTypes()
        {
           return _context.GiveHelpSubTypes.Include(g => g.GiveHelpType);
        }

        public void Remove(GiveHelpSubType GivehelpSubType)
        {
            _context.GiveHelpSubTypes.Remove(GivehelpSubType);
        }
    }
}