﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using BlessedHub.Core.Repoitories;
using BlessedHub.Core.RepositoriesInterfaces;
using BlessedHub.Models;
namespace BlessedHub.Presistence.Repositories
{
    public class UnitOfWork :IUnitOfWork
    {
        private readonly TheBlessedHubEntities _context;
      

        public IServiceRepository Services { get; private set; }
        public ICityRepository Cities { get; private set; }
        public IServiceTypeRepository ServiceTypes { get; private set; }
        public IOrganisationRepository Organisations { get; private set; }
        public IOrganisationServiceRepository OrganisationServices { get; private set; }
        public IOrganisationWorkingDaysRepository OrganisationWorkingDays { get; private set; }
        public IServiceTypeWorkingDaysDataRepository ServiceTypeWorkingDaysData { get; private set; }
        public IOrganisaitonServicesTypesRepository OrganisaitonServicesTypes { get; private set; }
        public IGivehelpSubTypeRepository givehelpSubTypeRepository { get; private set; }
        public IGiveHelpToOrganisaitonRepository giveHelpToOrganisaitonRepository { get; private set; }
        public IGiveHelpTypeRepository giveHelpTypeRepository { get; private set; }
        public IICanHelpRepository iCanHelpRepository { get; private set; }
        public INewsRepository newsRepository { get; private set; }
        public INewsSubTypeRepository newsSubTypeRepository { get; private set; }
        public INewsTypeRepository newsTypeRepository { get; private set; }
        public IVolunteerRepository volunteerRepository { get; private set; }
        public IWeekDayRepository weekDayRepository { get; private set; }
        public IEventsRepository eventsRepository { get; private set; }

        public ICountAll countAll { get; private set; }

        public IBlessedHubMembers blessedHubMembers { get; private set; }

        public IBlessedHubTeam blessedHubTeam { get; private set; }

        public IContactUsRepository contactUsRepository { get; private set; }

        public IPrayerTimingRepository prayerTimingRepository { get; private set; }

        public UnitOfWork(TheBlessedHubEntities context)
        {
            _context = context;
            Services = new ServiceRepository(context);
            Cities = new CityRepository(context);
            ServiceTypes = new ServiceTypeRepository(context);
            Organisations = new OrganisationRepository(context);
            OrganisationServices = new OrganisationServiceRepository(context);
            OrganisationWorkingDays = new OrganisationWorkingDaysRepository(context);
            ServiceTypeWorkingDaysData = new ServiceTypeWorkingDaysDataRepository(context);
            OrganisaitonServicesTypes = new OrganisaitonServicesTypesRepository(context);
            givehelpSubTypeRepository = new GivehelpSubTypeRepository(context);
            giveHelpToOrganisaitonRepository = new GiveHelpToOrganisaitonRepository(context);
            giveHelpTypeRepository = new GiveHelpTypeRepository(context);
            iCanHelpRepository = new ICanHelpRepository(context);
            newsRepository = new NewsRepository(context);
            newsSubTypeRepository = new NewsSubTypeRepository(context);
            newsTypeRepository = new NewsTypeRepository(context);
            volunteerRepository = new VolunteerRepository(context);
            weekDayRepository = new WeekDayRepository(context);
            countAll = new CountAll(context);
            blessedHubMembers = new BlessedHubMemberRepositories(context);
            blessedHubTeam = new BlessedHubTeamRepository(context);
            contactUsRepository = new ContactUsRepository(context);
            prayerTimingRepository = new PrayerTimingRepository(context);
            eventsRepository = new EventRepository(context);

        }

        public void Complete()
        {
            _context.SaveChanges();
        }

        public void DbStateModified(object obj)
        {
            _context.Entry(obj).State = EntityState.Modified;
        }
        private bool disposed = false;
        public void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }  

     
    }
}