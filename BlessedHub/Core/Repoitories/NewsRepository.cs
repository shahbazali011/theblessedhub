﻿using BlessedHub.Core.RepositoriesInterfaces;
using BlessedHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
namespace BlessedHub.Core.Repoitories
{
    public class NewsRepository : INewsRepository
    {
        private readonly TheBlessedHubEntities _context;
        public NewsRepository(TheBlessedHubEntities context)
        {
            _context = context;
        }
        public void Add(News News)
        {
           
                _context.News.Add(News);
            
        }

        public News GetNews(int? id)
        {
            return _context.News.Find(id);
        }

        public IEnumerable<News> GetNewss()
        {
            return _context.News.Include(n => n.NewsSubType).Include(n => n.NewsType);
        }

        public IEnumerable<News> GetNewsType(int? id)
        {
            return _context.News.Where(nt=>nt.NewsType.Id == id).Include(n => n.NewsSubType).Include(n => n.NewsType);
        }

        public void Remove(News News)
        {
            _context.News.Remove(News);
        }
    }
}