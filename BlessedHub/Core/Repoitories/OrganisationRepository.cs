﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BlessedHub.Models;
using System.Data.Entity;
using BlessedHub.Core.RepositoriesInterfaces;

namespace BlessedHub.Presistence.Repositories
{
    public class OrganisationRepository : IOrganisationRepository
    {
        private readonly TheBlessedHubEntities _context;
        public OrganisationRepository(TheBlessedHubEntities context)
        {
            _context = context;
        }

        public void Add(Organisation Organisation)
        { var record = _context.Organisations.Where(s => s.Name == Organisation.Name);
            if (record.Count() == 0)
            {
                _context.Organisations.Add(Organisation);
            }
        }

        public Organisation GetOrganisation(int? id)
        {
            return _context.Organisations.Include(c =>c.City1).SingleOrDefault(o => o.OrganisationId == id);
        }

        public IEnumerable<Organisation> GetOrganisations()
        {
            var organisations = _context.Organisations.Include(o => o.City1);

            return organisations;
        }

        public IEnumerable<Organisation> GetPostCodeList(string postcode)

        {
            //postcode = postcode.Substring(0, 3);
            var organisations = _context.Organisations.Where(pc =>pc.PostCode==postcode);

            return organisations;
        }

        public IEnumerable<Organisation> PartialMapOrganisation()
        {
            var organisations = _context.Organisations.Include(o => o.City1);

            return organisations;
        }

        public IEnumerable<Organisation> PartialMapOrganisationCity(int? id)
        {
            var organisations = _context.Organisations.Where(c=>c.CityId==id).Include(o => o.City1);

            return organisations;
        }

        public void Remove(Organisation Organisation)
        {
            _context.Organisations.Remove(Organisation);
        }
    }
}