﻿using BlessedHub.Core.RepositoriesInterfaces;
using BlessedHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
namespace BlessedHub.Core.Repoitories
{
    public class GiveHelpTypeRepository : IGiveHelpTypeRepository
    {
        private readonly TheBlessedHubEntities _context;
        public GiveHelpTypeRepository(TheBlessedHubEntities context)
        {
            _context = context;
        }
       
        public void Add(GiveHelpType GiveHelpType)
        {
            var record = _context.GiveHelpTypes.Where(s => s.HelpType == GiveHelpType.HelpType);
            if (record.Count() == 0)
            {
                _context.GiveHelpTypes.Add(GiveHelpType);
            }
        }

        public GiveHelpType GetGiveHelpType(int? id)
        {
            return _context.GiveHelpTypes.Find(id);
        }

        public IEnumerable<GiveHelpType> GetGiveHelpTypes()
        {
            return _context.GiveHelpTypes;
        }

        public void Remove(GiveHelpType GiveHelpType)
        {
            _context.GiveHelpTypes.Remove(GiveHelpType);
        }
    }
}