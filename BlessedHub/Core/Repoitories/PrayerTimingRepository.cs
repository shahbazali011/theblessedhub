﻿using BlessedHub.Core.RepositoriesInterfaces;
using BlessedHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlessedHub.Core.Repoitories
{
    public class PrayerTimingRepository : IPrayerTimingRepository
    {
        private readonly TheBlessedHubEntities _context;
        public PrayerTimingRepository(TheBlessedHubEntities context)
        {
            _context = context;
        }
        public void Add(PrayerTiming PrayerTiming)
        {
            _context.PrayerTimings.Add(PrayerTiming);
        }

        public PrayerTiming GetMasjidByName(string url)
        {
            return _context.PrayerTimings.FirstOrDefault(n=>n.CalendarPicURL==url);
        }

        public PrayerTiming GetPrayerTiming(int? id)
        {
            return _context.PrayerTimings.Find(id);
        }

        public IEnumerable<PrayerTiming> GetPrayerTimings()
        {
            return _context.PrayerTimings;
        }

        public void Remove(PrayerTiming PrayerTiming)
        {
            _context.PrayerTimings.Remove(PrayerTiming);
        }
    }
}