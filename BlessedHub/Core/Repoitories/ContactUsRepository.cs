﻿using BlessedHub.Core.RepositoriesInterfaces;
using BlessedHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlessedHub.Core.Repoitories
{
    public class ContactUsRepository : IContactUsRepository
    {
        private readonly TheBlessedHubEntities _context;
        public ContactUsRepository(TheBlessedHubEntities context)
        {
            _context = context;
        }
        public void Add(ContactU ContactU)
        {
            _context.ContactUs.Add(ContactU);
        }

        public ContactU GetContactU(int? id)
        {
            return _context.ContactUs.Find(id);
        }

        public IEnumerable<ContactU> GetContactUs()
        {
            return _context.ContactUs;
        }

        public void Remove(ContactU ContactU)
        {
            _context.ContactUs.Remove(ContactU);
        }
    }
}