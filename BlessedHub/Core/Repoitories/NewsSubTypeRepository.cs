﻿using BlessedHub.Core.RepositoriesInterfaces;
using BlessedHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
namespace BlessedHub.Core.Repoitories
{
    public class NewsSubTypeRepository : INewsSubTypeRepository
    {
        private readonly TheBlessedHubEntities _context;
        public NewsSubTypeRepository(TheBlessedHubEntities context)
        {
            _context = context;
        }
        public void Add(NewsSubType NewsSubType)
        {
            var record = _context.NewsSubTypes.Where(s => s.NewsSubType1 == NewsSubType.NewsSubType1);
            if (record.Count() == 0)
            {
                _context.NewsSubTypes.Add(NewsSubType);
            }
        }

        public NewsSubType GetNewsSubType(int? id)
        {
            return _context.NewsSubTypes.Find(id); 
        }

        public IEnumerable<NewsSubType> GetNewsSubTypes()
        {
            return _context.NewsSubTypes.Include(n => n.NewsType);
        }

        public void Remove(NewsSubType NewsSubType)
        {
            _context.NewsSubTypes.Remove(NewsSubType);
        }
    }
}