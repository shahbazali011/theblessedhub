﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BlessedHub.Models;
using System.Data.Entity;
using BlessedHub.Core.RepositoriesInterfaces;

namespace BlessedHub.Presistence.Repositories
{
    public class OrganisationServiceRepository : IOrganisationServiceRepository
    {
        private readonly TheBlessedHubEntities _context;
        public OrganisationServiceRepository(TheBlessedHubEntities context)
        {
            _context = context;
        }

        public IEnumerable<OrganisationService> OrganisationServicesAll(int? id)
        {
            var organisationServices = _context.OrganisationServices.Where(s => s.Service.ServiceId == id).Include(o => o.Organisation).Include(o => o.Service);

            return organisationServices;
        }

        public OrganisationService GetOrganisation(int id)
        {

            return _context.OrganisationServices.Find(id);
        }

        public IEnumerable<OrganisationService> GetOrganisationServices()
        {
            var organisationServices = _context.OrganisationServices.Include(o => o.Organisation).Include(o => o.Service);
            return organisationServices;
        }

        public OrganisationService GetOrganisationservice(int? id)
        {
            return _context.OrganisationServices.Find(id);
        }

        public void Add(OrganisationService OrganisationService)
        {
            var record = _context.OrganisationServices.Where(s => s.OrganisationId == OrganisationService.OrganisationId && s.ServiceId == OrganisationService.ServiceId);
            if (record.Count() == 0)
            {
                _context.OrganisationServices.Add(OrganisationService);
            }
        }

        public void Remove(OrganisationService OrganisationService)
        {
            _context.OrganisationServices.Remove(OrganisationService);
        }

        public IEnumerable<OrganisationService> GetOrganisationServicesMap(int? id)
        {
            var organisationServices = _context.OrganisationServices.Where(s=>s.ServiceId==id).Include(o => o.Organisation).Include(o => o.Service);
            return organisationServices;
        }

        public IEnumerable<OrganisationService> GetOrganisationServicesMapPostCode(int? id, string postcode)
        {
            var organisationServices = _context.OrganisationServices.Where(s => s.ServiceId == id && s.Organisation.PostCode.Contains(postcode)).Include(o => o.Organisation).Include(o => o.Service);
            return organisationServices;
        }
    }
}