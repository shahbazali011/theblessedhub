﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BlessedHub.Core.RepositoriesInterfaces;
using BlessedHub.Models;
namespace BlessedHub.Presistence.Repositories
{
    public class CityRepository : ICityRepository
    {
        private readonly TheBlessedHubEntities _context;
        public CityRepository(TheBlessedHubEntities context)
        {
            _context = context;
        }

        public void Add(City city)
        {
            var record = _context.Cities.Where(s => s.CityName == city.CityName);
            if (record.Count() == 0)
            {
                _context.Cities.Add(city);
            }
        }

        public IEnumerable<City> GetCities()
        {
            return _context.Cities;
        }

        public City GetCity(int? id)
        {
            return _context.Cities.SingleOrDefault(c => c.CityId == id);
        }

       

        public void Remove(City city)
        {
            _context.Cities.Remove(city);
        }
    }
}