﻿using BlessedHub.Core.RepositoriesInterfaces;
using BlessedHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
namespace BlessedHub.Core.Repoitories
{
    public class VolunteerRepository : IVolunteerRepository
    {
        private readonly TheBlessedHubEntities _context;
        public VolunteerRepository(TheBlessedHubEntities context)
        {
            _context = context;
        }

        public void Add(Volunteer Volunteer)
        {
            _context.Volunteers.Add(Volunteer);
        }

        public Volunteer GetVolunteer(int? id)
        {
            return _context.Volunteers.Find(id);
        }

        public IEnumerable<Volunteer> GetVolunteers()
        {
            return _context.Volunteers.Include(v => v.City); ;
        }

        public void Remove(Volunteer Volunteer)
        {
            _context.Volunteers.Remove(Volunteer);
        }
    }
}