﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using BlessedHub.Core.RepositoriesInterfaces;
using BlessedHub.Models;
namespace BlessedHub.Presistence.Repositories
{
    public class ServiceRepository : IServiceRepository
    {
        private readonly TheBlessedHubEntities _context;
        public ServiceRepository(TheBlessedHubEntities context)
        {
            _context = context;
        }

        public void Add(Service Service)
        {
            var record = _context.Services.Where(s => s.ServiceName == Service.ServiceName);
            if (record.Count() == 0)
            {
                _context.Services.Add(Service);
            }
        }

        public Service GetService(int? id)
        {
            return _context.Services.SingleOrDefault(s => s.ServiceId == id);
        }

        public IEnumerable<Service> GetServices()
        {
            return _context.Services;
        }

        public void Remove(Service Service)
        {
            _context.Services.Remove(Service);
        }
    }
}