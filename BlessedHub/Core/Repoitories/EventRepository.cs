﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BlessedHub.Core.RepositoriesInterfaces;
using BlessedHub.Models;
namespace BlessedHub.Core.Repoitories
{
    public class EventRepository : IEventsRepository
    {
        private readonly TheBlessedHubEntities _context;
        public EventRepository(TheBlessedHubEntities context)
        {
            _context = context;
        }
        public void Add(EventType eventy)
        {
            var record = _context.EventTypes.Where(s => s.EventTitle == eventy.EventTitle);
            if (record.Count() == 0)
            {
                _context.EventTypes.Add(eventy);
            }
        }

        public EventType GetEvent(int? id)
        {
            return _context.EventTypes.SingleOrDefault(i => i.Id == id);
        }

        public IEnumerable<EventType> GetEvents()
        {
            return _context.EventTypes;
        }

        public void Remove(EventType eventy)
        {
            _context.EventTypes.Remove(eventy);
        }
    }
}