﻿using BlessedHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlessedHub.Core.RepositoriesInterfaces
{
  public  interface IServiceTypeWorkingDaysDataRepository
    {
        IEnumerable<ServiceTypeWorkingDay> GetServiceTypeWorkingDays();
        ServiceTypeWorkingDay GetWorkingDay(int? id);
        IEnumerable<ServiceTypeWorkingDay> GetServiceTypeWorkingDays(int id);
        IEnumerable<ServiceTypeWorkingDay> PartialAllServicesTypesDetails(int? id);
        IEnumerable<ServiceTypeWorkingDay> PartialMapOrganisation(int? id);
        IEnumerable<ServiceTypeWorkingDay> PartialServiceTypeTimeTable(int? ServiceTypeId, int? ServiceId, int? OrganisationId);

        void Add(ServiceTypeWorkingDay ServiceTypeWorkingDay);
        void Remove(ServiceTypeWorkingDay ServiceTypeWorkingDay);

    }
}
