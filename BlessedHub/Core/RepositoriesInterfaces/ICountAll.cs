﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlessedHub.Core.RepositoriesInterfaces
{
   public interface ICountAll
    {
        int CountServices();
        int CountOrganisations();
        int CountNeeds();

    }
}
