﻿using BlessedHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlessedHub.Core.RepositoriesInterfaces
{
    public interface IBlessedHubTeam
    {
        IEnumerable<BleassedHubTeam> GetBleassedHubTeams();
        BleassedHubTeam GetBleassedHubTeam(int? id);
        void Add(BleassedHubTeam BleassedHubTeam);
        void Remove(BleassedHubTeam BleassedHubTeam);
    }
}
