﻿using BlessedHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlessedHub.Core.RepositoriesInterfaces
{
  public  interface IOrganisationServiceRepository
    {
        IEnumerable<OrganisationService> GetOrganisationServices();
        IEnumerable<OrganisationService> GetOrganisationServicesMap(int? id);
        IEnumerable<OrganisationService> GetOrganisationServicesMapPostCode(int? id,string postcode);
        OrganisationService GetOrganisationservice(int? id);
        IEnumerable<OrganisationService> OrganisationServicesAll(int? id);
        void Add(OrganisationService Organisation);
        void Remove(OrganisationService Organisation);

    }
}
