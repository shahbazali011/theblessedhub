﻿using BlessedHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlessedHub.Core.RepositoriesInterfaces
{
   public interface IVolunteerRepository
    {
        IEnumerable<Volunteer> GetVolunteers();
        Volunteer GetVolunteer(int? id);
        void Add(Volunteer Volunteer);
        void Remove(Volunteer Volunteer);
    }
}
