﻿using BlessedHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlessedHub.Core.RepositoriesInterfaces
{
    public interface IBlessedHubMembers
    {
        IEnumerable<BlessedHubMember> GetBlessedHubMembers();
        IEnumerable<BlessedHubMember> GetBlessedHubMembers(int? id);
        BlessedHubMember GetBlessedHubMember(int? id);
        void Add(BlessedHubMember BlessedHubMember);
        void Remove(BlessedHubMember BlessedHubMember);
    }
}
