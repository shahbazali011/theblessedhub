﻿using BlessedHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlessedHub.Core.RepositoriesInterfaces
{
  public  interface IContactUsRepository
    {
        IEnumerable<ContactU> GetContactUs();
        ContactU GetContactU(int? id);
        void Add(ContactU ContactU);
        void Remove(ContactU ContactU);
    }
}
