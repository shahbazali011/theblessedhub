﻿using BlessedHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlessedHub.Core.RepositoriesInterfaces
{
  public  interface IGivehelpSubTypeRepository
    {
        IEnumerable<GiveHelpSubType> GetGivehelpSubTypes();
        GiveHelpSubType GetGivehelpSubType(int? id);
        void Add(GiveHelpSubType GivehelpSubType);
        void Remove(GiveHelpSubType GivehelpSubType);
    }
}
