﻿using BlessedHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlessedHub.Core.RepositoriesInterfaces
{
  public  interface INewsTypeRepository
    {
        IEnumerable<NewsType> GetNewsTypes();
        NewsType GetNewsType(int? id);
        void Add(NewsType NewsType);
        void Remove(NewsType NewsType);
    }
}
