﻿using BlessedHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlessedHub.Core.RepositoriesInterfaces
{
  public  interface IServiceTypeRepository
    {
        IEnumerable<ServicesType> GetServicesTypes();
        ServicesType GetServicesType(int? id);
        void Add(ServicesType Service);
        void Remove(ServicesType Service);
        IEnumerable<ServicesType> GetServicesTypeSearch(string search);

    }
}
