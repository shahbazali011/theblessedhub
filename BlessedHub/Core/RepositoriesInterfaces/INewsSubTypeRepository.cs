﻿using BlessedHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlessedHub.Core.RepositoriesInterfaces
{
   public interface INewsSubTypeRepository
    {
        IEnumerable<NewsSubType> GetNewsSubTypes();
        NewsSubType GetNewsSubType(int? id);
        void Add(NewsSubType NewsSubType);
        void Remove(NewsSubType NewsSubType);
    }
}
