﻿using BlessedHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlessedHub.Core.RepositoriesInterfaces
{
    public interface IICanHelpRepository
    {
        IEnumerable<ICanHelp> GetICanHelps();
        ICanHelp GetICanHelp(int? id);
        void Add(ICanHelp ICanHelp);
        void Remove(ICanHelp ICanHelp);
    }
}