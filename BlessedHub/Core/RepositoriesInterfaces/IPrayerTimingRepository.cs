﻿using BlessedHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlessedHub.Core.RepositoriesInterfaces
{
    public interface IPrayerTimingRepository
    {
        IEnumerable<PrayerTiming> GetPrayerTimings();
        PrayerTiming GetPrayerTiming(int? id);
        PrayerTiming GetMasjidByName(string name);
        void Add(PrayerTiming PrayerTiming);
        void Remove(PrayerTiming PrayerTiming);
    }
}
