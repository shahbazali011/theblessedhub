﻿using BlessedHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlessedHub.Core.RepositoriesInterfaces
{
  public  interface IOrganisationWorkingDaysRepository
    {
        IEnumerable<OrganisationWorkingDay> GetOrganisationWorkingDays();
        OrganisationWorkingDay GetWorkingDay(int? id);
        IEnumerable<OrganisationWorkingDay> OranisationTimeTable(int? id);
        void Add(OrganisationWorkingDay OrganisationWorkingDay);
        void Remove(OrganisationWorkingDay OrganisationWorkingDay);
    }
}
