﻿using BlessedHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlessedHub.Core.RepositoriesInterfaces
{
   public interface INewsRepository
    {
        IEnumerable<News> GetNewss();
        IEnumerable<News> GetNewsType(int? id);
        News GetNews(int? id);
        void Add(News News);
        void Remove(News News);
    }
}
