﻿using BlessedHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlessedHub.Core.RepositoriesInterfaces
{
   public interface ICityRepository
    {
        IEnumerable<City> GetCities();
        City GetCity(int? id);
        void Add(City city);
        void Remove(City city);
    }
}
