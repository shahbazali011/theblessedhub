﻿using BlessedHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlessedHub.Core.RepositoriesInterfaces
{
   public interface IOrganisaitonServicesTypesRepository
    {
        IEnumerable<OrganisaitonServicesType> GetOrganisaitonServicesTypes();
        OrganisaitonServicesType GetOrganisaitonServicesType(int? id);
        IEnumerable<OrganisaitonServicesType> OrganisaitonServicesTypesAll(int? OrganisationId, int? OrganisationServicesId);
        IEnumerable<OrganisaitonServicesType> PartialServicesTypesCard(int? OrganisationId, int? OrganisationServicesId);
        //IEnumerable<OrganisaitonServicesType> GetOrganisaitonServicesTypes();
        //OrganisaitonServicesType GetOrganisaitonServicesType(int? id);
        void Add(OrganisaitonServicesType OrganisaitonServicesType);
        void Remove(OrganisaitonServicesType OrganisaitonServicesType);

    }
}
