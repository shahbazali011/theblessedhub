﻿using BlessedHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlessedHub.Core.RepositoriesInterfaces
{
   public interface IGiveHelpToOrganisaitonRepository
    {
        IEnumerable<GiveHelpToOrganisaiton> GetGiveHelpToOrganisaitons();
        IEnumerable<GiveHelpToOrganisaiton> GetGiveHelpToOrganisaitons(int? id);

        GiveHelpToOrganisaiton GiveHelpToOrganisaiton(int? id);
        void Add(GiveHelpToOrganisaiton GiveHelpToOrganisaiton);
        void Remove(GiveHelpToOrganisaiton GiveHelpToOrganisaiton);
    }
}
