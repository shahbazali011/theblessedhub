﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlessedHub.Models;

namespace BlessedHub.Core.RepositoriesInterfaces
{
    public interface IEventsRepository
    {
        IEnumerable<EventType> GetEvents();
        EventType GetEvent(int? id);
        void Add(EventType eventy);
        void Remove(EventType eventy);
    }
}
