﻿using BlessedHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlessedHub.Core.RepositoriesInterfaces
{
    public interface IWeekDayRepository
    {
        IEnumerable<WeekDay> GetWeekDays();
        WeekDay GetWeekDay(int? id);
        void Add(WeekDay WeekDay);
        void Remove(WeekDay WeekDay);
    }
}