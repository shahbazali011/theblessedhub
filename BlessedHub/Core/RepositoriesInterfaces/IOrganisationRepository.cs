﻿using BlessedHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlessedHub.Core.RepositoriesInterfaces
{
   public interface IOrganisationRepository
    {
        IEnumerable<Organisation> GetOrganisations();
        Organisation GetOrganisation(int? id);
        IEnumerable<Organisation> GetPostCodeList(string postcode);
        IEnumerable<Organisation> PartialMapOrganisation();
        IEnumerable<Organisation> PartialMapOrganisationCity(int? id);
        void Add(Organisation Organisation);
        void Remove(Organisation Organisation);
    }
}
