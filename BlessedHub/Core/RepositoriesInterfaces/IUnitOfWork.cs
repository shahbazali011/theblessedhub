﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlessedHub.Core.RepositoriesInterfaces
{
  public  interface IUnitOfWork 
    {
        IServiceRepository Services { get; }
        ICityRepository Cities { get; }
        IOrganisationRepository Organisations { get; }
        IServiceTypeRepository ServiceTypes { get; }
        IOrganisationServiceRepository OrganisationServices { get; }
        IOrganisationWorkingDaysRepository OrganisationWorkingDays { get; }
        IServiceTypeWorkingDaysDataRepository ServiceTypeWorkingDaysData { get; }
        IOrganisaitonServicesTypesRepository OrganisaitonServicesTypes { get; }
        IGivehelpSubTypeRepository givehelpSubTypeRepository { get; }
        IGiveHelpToOrganisaitonRepository giveHelpToOrganisaitonRepository { get; }
        IGiveHelpTypeRepository giveHelpTypeRepository { get; }
        IICanHelpRepository iCanHelpRepository { get; }
        INewsRepository newsRepository { get; }
        INewsSubTypeRepository newsSubTypeRepository { get; }
        INewsTypeRepository newsTypeRepository { get; }
        IVolunteerRepository volunteerRepository { get; }
        IWeekDayRepository weekDayRepository { get; }
        ICountAll countAll { get; }
        IBlessedHubMembers blessedHubMembers { get; }
        IBlessedHubTeam blessedHubTeam { get; }
        IContactUsRepository contactUsRepository { get; }
        IPrayerTimingRepository prayerTimingRepository { get; }
        IEventsRepository eventsRepository { get; }



        void DbStateModified(object obj);

        void Complete();

       void Dispose(bool disposing);
        void Dispose();
    }
}
