﻿using BlessedHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlessedHub.Core.RepositoriesInterfaces
{
 public   interface IGiveHelpTypeRepository
    {
        IEnumerable<GiveHelpType> GetGiveHelpTypes();
        GiveHelpType GetGiveHelpType(int? id);
        void Add(GiveHelpType GiveHelpType);
        void Remove(GiveHelpType GiveHelpType);
    }
}
