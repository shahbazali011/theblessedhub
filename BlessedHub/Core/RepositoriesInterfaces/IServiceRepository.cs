﻿using BlessedHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlessedHub.Core.RepositoriesInterfaces
{
  public interface IServiceRepository
    {
        IEnumerable<Service> GetServices();
        Service GetService(int? id);
        void Add(Service Service);
        void Remove(Service Service);
    }
}
