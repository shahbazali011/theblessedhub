//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BlessedHub.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class BleassedHubTeam
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BleassedHubTeam()
        {
            this.BlessedHubMembers = new HashSet<BlessedHubMember>();
        }
    
        public int Id { get; set; }
        public string TeamTitle { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BlessedHubMember> BlessedHubMembers { get; set; }
    }
}
