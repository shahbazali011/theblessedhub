﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BlessedHub.Models;
using BlessedHub.ViewModels;
namespace BlessedHub.App_Start
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<City, CityVM>();
            CreateMap<GiveHelpSubType, GiveHelpSubTypeVM>();
            CreateMap<GiveHelpToOrganisaiton, GiveHelpToOrganisaitonVM>();
            CreateMap<GiveHelpType,GiveHelpTypeVM>();
            CreateMap<ICanHelp, ICanHelpVM>();
            CreateMap<NewsSubType, NewsSubTypeVM>();
            CreateMap<NewsType,NewsTypeVM>();
            CreateMap<News,NewsVM>();
            CreateMap<OrganisaitonServicesType,OrganisaitonServicesTypeVM>();
            CreateMap<OrganisationService,OrganisationServiceVM>();
            CreateMap<Organisation,OrganisationVM>();
            CreateMap<OrganisationWorkingDay,OrganisationWorkingDayVM>();
            CreateMap<ServicesType,ServicesTypeVM>();
            CreateMap<ServiceTypeWorkingDay,ServiceTypeWorkingDayVM>();
            CreateMap<Service,ServiceVM>();
            CreateMap<Volunteer,VolunteerVM>();
            CreateMap<WeekDay,WeekDayVM>();
            CreateMap<PrayerTiming, PrayerTimingVM>();

        }
    }
}