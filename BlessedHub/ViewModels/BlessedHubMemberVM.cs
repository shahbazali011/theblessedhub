﻿using BlessedHub.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BlessedHub.ViewModels
{
    public class BlessedHubMemberVM
    {
        public int Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string JobTitle { get; set; }
        [Required]
        public string JobDescription { get; set; }
        [Required]
        public int TeamId { get; set; }
        [Required]
        public string Mobiel { get; set; }
        public string Phone { get; set; }
        [Required]
        public string Role { get; set; }
        [Required]
        public string Acievement { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string Postcode { get; set; }
        [Required]
        public string City { get; set; }
        public Nullable<int> RoleId { get; set; }
        public Nullable<bool> Active { get; set; }
        public Nullable<System.DateTime> DateAdded { get; set; }
        public string DateUpdated { get; set; }
        public string PictureUrl { get; set; }

        [Required]
        [Display(Name = "Upload File")]
        public HttpPostedFileBase FileAttach { get; set; }

        public BleassedHubTeam BleassedHubTeam { get; set; }
    }
}