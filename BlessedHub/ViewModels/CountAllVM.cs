﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlessedHub.ViewModels
{
    public class CountAllVM
    {
        public int CountServices { get; set; }
        public int CountOrganisations { get; set; }
        public int CountNeeds { get; set; }
    }
}