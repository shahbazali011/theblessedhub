﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using BlessedHub.Models;

namespace BlessedHub.ViewModels
{
    public class GiveHelpToOrganisaitonVM
    {
        public int Id { get; set; }
        [DisplayName("Give help subtype")]
        [Required(ErrorMessage = "Please enter give Help subType")]
        public int GiveHelpSubTypesId { get; set; }
        [DisplayName("Organisation")]
        [Required(ErrorMessage = "Please enter OrganisationId")]
        public int OrganisationId { get; set; }
        [DisplayName("Description")]
        [Required(ErrorMessage = "Please enter Description")]
        public string Description { get; set; }
        [DisplayName("Give Help Types")]
        [Required(ErrorMessage = "Please enter Give Help Types Id")]
        public int GiveHelpTypesId { get; set; }
        [DisplayName("Date Added")]
   
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> DateAdded { get; set; }
        [DisplayName("Active")]
        [Required(ErrorMessage = "Please enter Active")]
        public Nullable<bool> Active { get; set; }
        [DisplayName("Date Updated")]
       
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> DateUpdated { get; set; }

        public GiveHelpSubType GiveHelpSubType { get; set; }
        public GiveHelpType GiveHelpType { get; set; }
        public Organisation Organisation { get; set; }
    }
}