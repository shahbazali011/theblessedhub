﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using BlessedHub.Models;

namespace BlessedHub.ViewModels
{
    public class ServicesTypeVM
    {
        public int ServiceTypeId { get; set; }

        [DisplayName("Service Type")]
        [Required(ErrorMessage = "Please enter Service Type")]
        public string ServiceType { get; set; }

        [DisplayName("Services")]
        [Required(ErrorMessage = "Please enter Service")]
        public int ServiceId { get; set; }

        [DisplayName("Description ")]
        [Required(ErrorMessage = "Please enter Description")]
        public string Description { get; set; }

        [DisplayName("Active ")]
        [Required(ErrorMessage = "Please enter Active")]
        public Nullable<bool> Active { get; set; }

       
        public Service Service { get; set; }

    }
}