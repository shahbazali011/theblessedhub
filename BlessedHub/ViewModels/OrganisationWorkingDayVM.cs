﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using BlessedHub.Models;

namespace BlessedHub.ViewModels
{
    public class OrganisationWorkingDayVM
    {
        public int Id { get; set; }
        [DisplayName("Day ")]
        [Required(ErrorMessage = "Please enter Day")]
        public Nullable<int> DayId { get; set; }
        [DisplayName("Organisation ")]
        [Required(ErrorMessage = "Please enter Organisation")]
        public Nullable<int> OrganisationId { get; set; }
        [DisplayName("Working Day ")]
        [Required(ErrorMessage = "Please enter Working Day")]
        public Nullable<bool> WorkingDay { get; set; }
        [DisplayName("Start Time ")]
        [Required(ErrorMessage = "Please enter Start Time")]
        public string StartTime { get; set; }
        [DisplayName("End Time ")]
        [Required(ErrorMessage = "Please enter End Time")]
        public string EndTime { get; set; }

        public Organisation Organisation { get; set; }
        public WeekDay WeekDay { get; set; }
    }
}