﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BlessedHub.ViewModels
{
    public class CityVM
    {
        public int CityId { get; set; }

        [DisplayName("City Name")]
        [Required(ErrorMessage = "Please enter city name")]
        public string CityName { get; set; }

        [DisplayName("Date Added")]
      
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> DateAdded { get; set; }

        [Required(ErrorMessage = "Please check active")]
        public Nullable<bool> Active { get; set; }
        [DisplayName("Date updated")]
     
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> DateUpdated { get; set; }
    }
}