﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BlessedHub.ViewModels
{
    public class WeekDayVM
    {
        public int Id { get; set; }
        [DisplayName("Days ")]
        [Required(ErrorMessage = "Please Enter " +
            "Dayse")]
        public string Days { get; set; }
    }
}