﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using BlessedHub.Models;

namespace BlessedHub.ViewModels
{
    public class NewsVM
    {
        public int Id { get; set; }
        [DisplayName("News Title")]
        [Required(ErrorMessage = "Please enter Mews Title")]
        public string NewsTitle { get; set; }

        [DisplayName("News Detail")]
        [Required(ErrorMessage = "Please enter News Detail")]
        public string NewsDetail { get; set; }

        [DisplayName("More Details")]
        [Required(ErrorMessage = "Please enter More Details")]
        public string MoreDetails { get; set; }

        [DisplayName("Posted Date")]

        public string PostedDate { get; set; }

        [DisplayName("Posted By")]
        //[Required(ErrorMessage = "Please enter Posted By")]
        public string PostedBy { get; set; }

        [DisplayName("New Type")]
        [Required(ErrorMessage = "Please enter New Type")]
        public int NewsTypeId { get; set; }

        [DisplayName("New SubType")]
        [Required(ErrorMessage = "Please enter New SubType")]
        public int NewsSubTypeId { get; set; }

        [DisplayName("Facebook link")]
        [Required(ErrorMessage = "Please enter Facebook link")]
        public string FacebookLink { get; set; }

        [DisplayName("Twitter link")]
        [Required(ErrorMessage = "Please enter Twitter link")]
        public string TwitterLink { get; set; }

        [DisplayName("Tagged")]
        [Required(ErrorMessage = "Please enter Tagged")]
        public string Tagged { get; set; }

        [DisplayName("Picture URL")]
        //[Required(ErrorMessage = "Please enter PictureURL")]
        public string PictureURL { get; set; }

        //[Required]
        [Display(Name = "Upload File")]
        public HttpPostedFileBase FileAttach { get; set; }

        public Nullable<bool> Active { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> DateAdded { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> DateUpdated { get; set; }

        public NewsSubType NewsSubType { get; set; }

        public NewsType NewsType { get; set; }
    }
}