﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BlessedHub.ViewModels
{
    public class ServiceVM
    {
      
        public int ServiceId { get; set; }
        [DisplayName("Service Name ")]
        [Required(ErrorMessage = "Please enter Service Name")]
        public string ServiceName { get; set; }
        [DisplayName("Description ")]
        [Required(ErrorMessage = "Please enter Description")]
        public string Description { get; set; }
        [DisplayName("Active ")]
        [Required(ErrorMessage = "Please Enter Active")]
        public Nullable<bool> Active { get; set; }
    }
}