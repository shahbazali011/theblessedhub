﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BlessedHub.ViewModels
{
    public class NewsTypeVM
    {
        public int Id { get; set; }
        [DisplayName("New Type1")]
        [Required(ErrorMessage = "Please enter New Type1")]
        public string NewsType1 { get; set; }
        [DisplayName("Description")]
        [Required(ErrorMessage = "Please enter Description")]
        public string Description { get; set; }
    }
}