﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using BlessedHub.Models;

namespace BlessedHub.ViewModels
{
    public class OrganisationVM
    {
        public int OrganisationId { get; set; }
        public int CityId { get; set; }
        [DisplayName("Name ")]
        [Required(ErrorMessage = "Please enter Name")]
        public string Name { get; set; }
        [DisplayName("Type ")]
        [Required(ErrorMessage = "Please enter Type")]
        public string Type { get; set; }
        [DisplayName("Title ")]
        [Required(ErrorMessage = "Please enter Title")]
        public string Title { get; set; }
        [DisplayName("Subtitle ")]
      
        public string SubTitle { get; set; }
        [DisplayName("Description ")]
        [Required(ErrorMessage = "Please enter Description")]
        public string Description { get; set; }
        [DisplayName("Address1 ")]
        [Required(ErrorMessage = "Please enter Address1")]
        public string Address1 { get; set; }
        [DisplayName("Address2 ")]
        
        public string Address2 { get; set; }
        [DisplayName("Address3 ")]
       
        public string Address3 { get; set; }
        [DisplayName("Postcode ")]
        [Required(ErrorMessage = "Please enter Postcode")]
        public string PostCode { get; set; }
        [DisplayName("Latitude ")]       
        public Nullable<decimal> Latitude { get; set; }
        [DisplayName("Longitude ")]      
        public Nullable<decimal> Longitude { get; set; }
        [DisplayName("City ")]
  
        public string City { get; set; }
        [DisplayName("Telephone ")]     
        public string Telephone { get; set; }
        [DisplayName("Email ")]
        public string Email { get; set; }
        [DisplayName("Website ")]       
        public string Website { get; set; }
        [DisplayName("Twitter ")]       
        public string Twitter { get; set; }
        [DisplayName("Facebook ")]      
        public string Facebook { get; set; }
        [DisplayName("Active ")]       
        public Nullable<bool> Active { get; set; }
        [DisplayName("Date Added ")]     
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> DateAdded { get; set; }
        [DisplayName("Date Updated ")]     
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> DateUpdated { get; set; }

        public City City1 { get; set; }
    }
}