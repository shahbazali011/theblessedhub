﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BlessedHub.ViewModels
{
    public class GiveHelpTypeVM
    {
        public int Id { get; set; }
        [DisplayName("Help Type")]
        [Required(ErrorMessage = "Please enter Help Type")]
        public string HelpType { get; set; }
        [DisplayName("Description")]
        [Required(ErrorMessage = "Please enter Description")]
        public string Description { get; set; }
    }
}