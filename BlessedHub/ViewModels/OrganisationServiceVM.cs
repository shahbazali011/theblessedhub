﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using BlessedHub.Models;

namespace BlessedHub.ViewModels
{
    public class OrganisationServiceVM
    {
        public int OrganisationServicesId { get; set; }
        [DisplayName("Organisation ")]
        [Required(ErrorMessage = "Please enter Organisation")]
        public int OrganisationId { get; set; }
        [DisplayName("Description ")]
        [Required(ErrorMessage = "Please enter Description")]
        public string Description { get; set; }
        [DisplayName("Service ")]
        [Required(ErrorMessage = "Please enter Service")]
        public int ServiceId { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> DateAdded { get; set; }
        public Nullable<bool> Active { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> DateUpdated { get; set; }

        public Organisation Organisation { get; set; }
        public Service Service { get; set; }
    }
}