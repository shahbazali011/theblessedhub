﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using BlessedHub.Models;

namespace BlessedHub.ViewModels
{
    public class VolunteerVM
    {
        public int Id { get; set; }
        [DisplayName("First Name ")]
        [Required(ErrorMessage = "Please Enter First Name")]
        public string FirstName { get; set; }
        [DisplayName("Last Name ")]
        [Required(ErrorMessage = "Please Enter Last Name")]
        public string LastName { get; set; }
        [DisplayName("Email Address ")]
        [Required(ErrorMessage = "Please Enter Email Address")]
        public string EmailAddress { get; set; }
        [DisplayName("Telephone Number ")]
        [Required(ErrorMessage = "Please Enter Telephone Number")]
        public string TelephoneNumber { get; set; }
        [DisplayName("City ")]
        [Required(ErrorMessage = "Please Enter City")]
        public int CityId { get; set; }
        [DisplayName("PostCode ")]
        [Required(ErrorMessage = "Please Enter PostCode")]
        public string PostCode { get; set; }
        [DisplayName("Skills ")]
        [Required(ErrorMessage = "Please Enter Skills")]
        public string Skills { get; set; }
        [DisplayName("Availability ")]
        [Required(ErrorMessage = "Please Enter Availability")]
        public string Availablity { get; set; }
        [DisplayName("Resources ")]
        [Required(ErrorMessage = "Please Enter Resources")]
        public string Resources { get; set; }
        [DisplayName("Date Added")]

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> DateAdded { get; set; }
        [DisplayName("Active ")]
        [Required(ErrorMessage = "Please Enter Active")]
        public Nullable<bool> Active { get; set; }
        [DisplayName("Date Updated ")]
  
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> DateUpdated { get; set; }

        public City City { get; set; }
    }
}