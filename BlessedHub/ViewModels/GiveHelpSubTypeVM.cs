﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using BlessedHub.Models;

namespace BlessedHub.ViewModels
{
    public class GiveHelpSubTypeVM
    {
        public int Id { get; set; }

        [DisplayName("Help Types")]
        public int GiveHelpTypesId { get; set; }

        [DisplayName("Help SubType")]
        [Required(ErrorMessage = "Please enter Help subType")]
        public string HelpSubType { get; set; }

        [DisplayName("Description")]
        [Required(ErrorMessage = "Please enter Description")]
        public string Description { get; set; }

        public GiveHelpType GiveHelpType { get; set; }


    }
}