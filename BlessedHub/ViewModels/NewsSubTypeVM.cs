﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using BlessedHub.Models;

namespace BlessedHub.ViewModels
{
    public class NewsSubTypeVM
    {
        public int Id { get; set; }
        public int NewsTypeId { get; set; }
        [DisplayName("New SubType1")]
        [Required(ErrorMessage = "Please enter New SubType1")]
        public string NewsSubType1 { get; set; }
        [DisplayName("Description")]
        [Required(ErrorMessage = "Please enter Description")]
        public string Description { get; set; }

        public NewsType NewsType { get; set; }
    }
}