﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using BlessedHub.Models;

namespace BlessedHub.ViewModels
{
    public class ServiceTypeWorkingDayVM
    {
        public int Id { get; set; }
        [DisplayName("Day ")]
        [Required(ErrorMessage = "Please enter Day")]
        public int DayId { get; set; }
        [DisplayName("Service ")]
        [Required(ErrorMessage = "Please enter Service")]
        public int ServiceId { get; set; }
        [DisplayName("Service Type ")]
        [Required(ErrorMessage = "Please enter Service Type")]
        public int ServiceTypeId { get; set; }
        [DisplayName("Organisation ")]
        [Required(ErrorMessage = "Please enter Organisation")]
        public int OrganisationId { get; set; }
        [DisplayName("Working Day ")]
        [Required(ErrorMessage = "Please enter Working Day")]
        public Nullable<bool> WorkingDay { get; set; }
        [DisplayName("Start Time ")] 
        [Required(ErrorMessage = "Please enter Start Time")]
        public string StartTime { get; set; }
        [DisplayName("End Time ")]   
        [Required(ErrorMessage = "Please enter End Time")]
        public string EndTime { get; set; }

        public Organisation Organisation { get; set; }
        public Service Service { get; set; }
        public ServicesType ServicesType { get; set; }
        public WeekDay WeekDay { get; set; }
    }
}