﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using BlessedHub.Models;

namespace BlessedHub.ViewModels
{
    public class ICanHelpVM
    {
        public int Id { get; set; }
        public int GiveHelpToOrganisaitonId { get; set; }
        [DisplayName("Email")]
        [Required(ErrorMessage = "Please enter Email")]
        public string Email { get; set; }
        [DisplayName("Message")]
        [Required(ErrorMessage = "Please enter Message")]
        public string Message { get; set; }
        [DisplayName("Description")]
        [Required(ErrorMessage = "Please enter Description")]
        public string Descrption { get; set; }

        public GiveHelpToOrganisaiton GiveHelpToOrganisaiton { get; set; }

    }
}