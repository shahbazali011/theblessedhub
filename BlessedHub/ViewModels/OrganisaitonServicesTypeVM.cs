﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using BlessedHub.Models;

namespace BlessedHub.ViewModels
{
    public class OrganisaitonServicesTypeVM
    {
        public int OrganisaitonServicesTypesId { get; set; }
        [DisplayName("Service type ")]
        [Required(ErrorMessage = "Please enter Service type")]
        public int ServiceTypeId { get; set; }
        [DisplayName("Organisation")]
        [Required(ErrorMessage = "Please enter organisation")]
        public int OrganisationId { get; set; }
        [DisplayName("Suitable For")]
        [Required(ErrorMessage = "Please enter Suitable for")]
        public string SuitableFor { get; set; }
        [DisplayName("Description")]
        [Required(ErrorMessage = "Please enter Description")]
        public string Description { get; set; }
        [DisplayName("Service")]
        [Required(ErrorMessage = "Please enter Service")]
        public int ServiceId { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> DateAdded { get; set; }
        public Nullable<bool> Active { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> DateUpdated { get; set; }

        public Organisation Organisation { get; set; }
        public Service Service { get; set; }
        public ServicesType ServicesType { get; set; }

    }
}